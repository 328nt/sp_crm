<?php

use Illuminate\Database\Seeder;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 30;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('students')->insert([
                'school_id' => 1,
                'unit_class_id' => 2,
                'image' => '8hL0-3.jpg',
                'dob' => '06/10/2006',
                'fullname' => $faker->name,
                'dad_name' => $faker->name,
                'dad_email' => $faker->email,
                'dad_phone' => $faker->phoneNumber,
                'mom_name' => $faker->name,
                'mom_email' => $faker->email,
                'mom_phone' => $faker->phoneNumber,
            ]);
        }
    }
}
