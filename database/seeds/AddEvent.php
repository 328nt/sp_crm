<?php

use Illuminate\Database\Seeder;

class AddEvent extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 30;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('event_calendars')->insert([
                'user_id' => 13,
                'title' => $faker->name,
                'start_date' => $faker->name,
                'end_date' => $faker->name,
            ]);
        }
    }
}
