<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInforTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infor_teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->double('starting_date', 15, 1)->nullable();
            $table->string('position', 100)->nullable();
            $table->string('rate_sp', 100)->nullable();
            $table->string('rate_center', 100)->nullable();
            $table->string('personal_email', 100)->nullable();
            $table->string('contact', 20)->nullable();
            $table->string('nationality', 100)->nullable();
            $table->string('passport', 20)->nullable();
            $table->string('dob', 20)->nullable();
            $table->longText('background')->nullable();
            $table->integer('school_id')->unsigned()->nullable();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->string('address', 255)->nullable();
            $table->string('state_health_insurance', 20)->nullable();
            $table->string('bussiness_visa', 20)->nullable();
            $table->string('bv_duration', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infor_teachers');
    }
}
