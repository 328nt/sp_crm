<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->unsigned()->nullable();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->integer('unit_class_id')->unsigned()->nullable();
            $table->foreign('unit_class_id')->references('id')->on('unit_classes')->onDelete('cascade');
            $table->string('fullname', 100)->nullable()->default('text');
            $table->date('dob')->nullable();
            $table->string('dad_name', 100)->nullable()->default('text');
            $table->double('dad_phone')->nullable()->default(123.4567);
            $table->string('dad_email', 100)->nullable();
            $table->string('mom_name', 100)->nullable()->default('text');
            $table->double('mom_phone')->nullable()->default(123.4567);
            $table->string('mom_email', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
