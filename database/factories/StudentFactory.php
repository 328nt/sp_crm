<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'fullname' => $faker->name,
        'school_id' => 1,
        'unit_class_id' => 1,
        'image' => 'default.png',
        'dad_name' => $faker->name,
        'mom_name' => $faker->name,
        'dad_email' => $faker->unique()->safeEmail,
        'mom_email' => $faker->unique()->safeEmail,
        'dad_phone' => $faker->phoneNumber,
        'mom_phone' => $faker->phoneNumber,
        'created_at' => new DateTime,
        'updated_at' => new DateTime,
    ];
});
