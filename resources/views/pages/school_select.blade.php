<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>select schools</title>
  <base href="{{asset('')}}">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- favicon
		============================================ -->
  <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
  <!-- Google Fonts
		============================================ -->
  <link href="https://fonts.googleapis.com/css?family=Play:400,700" rel="stylesheet">
  <!-- Bootstrap CSS
		============================================ -->
  <link rel="stylesheet" href="be/css/bootstrap.min.css">
  <!-- Bootstrap CSS
		============================================ -->
  <link rel="stylesheet" href="be/css/font-awesome.min.css">
  <!-- forms CSS
		============================================ -->
  <link rel="stylesheet" href="be/css/form/all-type-forms.css">
  <!-- modals CSS
		============================================ -->
  <link rel="stylesheet" href="be/css/modals.css">
  <!-- style CSS
		============================================ -->
  <link rel="stylesheet" href="be/style.css">
  <!-- responsive CSS
		============================================ -->
  <link rel="stylesheet" href="be/css/responsive.css">
</head>

<body>
  <!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
  <div class="error-pagewrap">
    <div class="error-page-int">

      @if(session('msg'))
      <div class="alert alert-danger alert-dismissible fade in">
        {{ session('msg') }}
      </div>
      @endif
      <div class="text-center m-b-md custom-login">
        <h3>PLEASE SELECT YOUR SCHOOL</h3>
      </div>
      <div class="content-error">
        <div class="hpanel">
          <div class="panel-body">
            <form action="{{route('school_selected')}}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <select name="school" id="" class="form-control">
                  @foreach ($schools as $school)
                  <option value="{{$school->id}}">{{$school->name}}</option>
                  @endforeach
                </select>
              </div>
              <input type="submit" class="btn btn-success btn-block loginbtn" value="Select">
            </form>
          </div>
        </div>
      </div>

      <div class="modal-bootstrap shadow-inner mg-tb-30 responsive-mg-b-0">
        <div class="modal-area-button">
          <a class="Information Information-color mg-b-10" href="#" data-toggle="modal"
            data-target="#school_store">Add new school</a>
        </div>
      </div>
      <div id="school_store" class="modal modal-edu-general fullwidth-popup-InformationproModal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-close-area modal-close-df">
              <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
            </div>
            <div class="modal-body">
              <span class="educate-icon educate-info modal-check-pro information-icon-pro"> </span>
              <h2>add new School!</h2>
            <form action="{{route('school_store')}}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
                <div class="row">
                  <div class="form-group text-left col-md-12">
                    <label for="Name">Name:</label>
                    <input type="text" class="form-control" name="name" id="name">
                  </div>
                </div>
                <div class="row">
                  <div class="form-group text-left col-md-12">
                    <label for="Address">Address:</label>
                    <input type="text" class="form-control" name="address" id="address">
                  </div>
                </div>
                <div class="row">
                  <div class="form-group text-left col-md-12" id="data_1">
                    <label for="Founding">Founding (mm/dd/yy):</label>
                    <div class="input-group date">
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                      <input type="text" class="form-control" name="founding" value="{{date('m/d/Y')}}">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group text-left col-md-12">
                    <label for="Event">Event:</label>
                    <textarea name="event" id="event" cols="30" rows="5"></textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group text-left col-md-12">
                    <label for="">image</label>
                    <input class="form-control" name="image" id="image" type="file">
                  </div>
                </div>
                <div class="row">
                  <button type="submit"
                  class="btn btn-primary waves-effect waves-light">Submit</button>
                </div>
              </form>
            </div>
            <div class="modal-footer info-md">
              <a data-dismiss="modal" href="#">Cancel</a>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

<!-- jquery
		============================================ -->
<script src="be/js/vendor/jquery-1.12.4.min.js"></script>
<!-- bootstrap JS
		============================================ -->
<script src="be/js/bootstrap.min.js"></script>

</html>