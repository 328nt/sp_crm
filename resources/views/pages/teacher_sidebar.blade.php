<div class="left-sidebar-pro">
    <nav id="sidebar" class="">
        <div class="sidebar-header">
            <a href="index.html"><img class="main-logo" src="be/img/logo-w.png" alt="" /></a>
            <strong><a href="index.html"><img src="be/img/logo-w.png" alt="" /></a></strong>
        </div>
        <div class="left-custom-menu-adp-wrap comment-scrollbar">
            <nav class="sidebar-nav left-sidebar-menu-pro">
                <ul class="metismenu" id="menu1">
                    <li class="{{ request()->route()->named('teacher_info*') ? 'active current' : '' }}" >
                        <a title="Landing Page" href="{{route('teacher_info')}}" aria-expanded="false"><span
                                class="educate-icon educate-professor icon-wrap sub-icon-mg" aria-hidden="true"></span>
                            <span class="mini-click-non">Home Page</span></a>
                    </li>
                    <li class="{{ request()->route()->named('teacher_info*') ? 'active current' : '' }}" >
                        <a title="Landing Page" href="{{route('teacher_info')}}" aria-expanded="false"><span
                                class="educate-icon educate-event icon-wrap sub-icon-mg" aria-hidden="true"></span>
                            <span class="mini-click-non">Manager Programs</span></a>
                    </li>
                    <li class="{{ request()->route()->named('select_school*') ? 'active current' : '' }}" >
                        <a title="Landing Page" href="{{route('select_school')}}" aria-expanded="false"><span
                                class="educate-icon educate-event icon-wrap sub-icon-mg" aria-hidden="true"></span>
                            <span class="mini-click-non">Manager training</span></a>
                    </li>
                    <li class="{{ request()->route()->named('feedback*') ? 'active current' : '' }}" >
                        <a title="Landing Page" href="{{route('feedback')}}" aria-expanded="false"><span
                                class="educate-icon educate-event icon-wrap sub-icon-mg" aria-hidden="true"></span>
                            <span class="mini-click-non">Feedback</span></a>
                    </li>
                </ul>
            </nav>
        </div>
    </nav>
</div>