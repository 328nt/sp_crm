@extends('pages/teacher_index')
@section('title')
class Info
@endsection
@section('content')

<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <form role="search" class="sr-input-func">
                                    <input type="text" placeholder="Search..." class="search-int form-control">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Single pro tab review Start-->
        <div class="blog-details-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="blog-details-inner">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="latest-blog-single blog-single-full-view">
                                        <div class="blog-image">
                                            <img src="upload/classes/{{$class->image}}" alt="" />
                                            <div class="blog-date">
                                                <p><span class="blog-day">{{$class->name}}</span></p>
                                            </div>
                                        </div>
                                        <div class="blog-details blog-sig-details">
                                            <div class="details-blog-dt blog-sig-details-dt courses-info mobile-sm-d-n">
                                                <span><a href="#"><i class="fa fa-user"></i> <b>{{$class->program->name}} program</b></a></span>
                                                <span><a href="#"><i class="fa fa-user"></i> <b>Gv chủ nhiệm:</b> Someone</a></span>
                                            <span><a href="#list_student"><i class="fa fa-heart"></i> <b>Sĩ số:</b> {{count($class->student)}}</a></span>
                                                <span><a href="#"><i class="fa fa-comments-o"></i> <b>Professor Name:</b> Alva Adition</a></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        
                        <h1><a class="blog-ht" href="#">Class Info Dummy Title</a></h1>
                        {!!$class->info!!}
                    </div>
                                
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        
                        <h1>List student</h1>
                        
                        
                        <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                        data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                        <thead>
                            <tr>
                                <th data-field="state" data-checkbox="true"></th>
                                <th data-field="id">ID</th>
                                <th data-field="fullname" data-editable="false">FullName</th>
                                <th data-field="image" data-editable="false">Image</th>
                                <th data-field="dob" data-editable="false">Dob</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($class->student as $student)
                            <tr>
                                <td></td>
                                <td>{{$student->id}}</td>
                                <td>{{$student->fullname}}</td>
                                <td style="text-align: center"><img src="upload/students/{{$student->image}}" width="50px" alt=""></td>
                                <td>{{$student->dob}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                        {{-- @foreach ($class->student as $student)
                        <div class="row" style="min-height: 130px;">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="user-comment ">
                                    <img src="upload/students/{{$student->image}}" width="50px" alt="" />
                                    <div class="comment-details">
                                        <h4>{{$student->fullname}} <span class="comment-replay">{{date('d-m-Y',strtotime($student->dob))}}</span></h4>
                                        <p>{!!$comment->comment!!}</p> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach --}}
                    </div>
                            </div>
                            
                            <hr />
                            <div class="row mg-b-15">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="skill-title">
                                                <h2>Skill Set</h2>
                                                <hr />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="comment-head lead-title">
                                                <h3>Home work</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="progress-skill">
                                        <h2>Homework</h2>
                                        <div class="progress progress-mini">
                                            <div style="width: 90%;" class="progress-bar progress-yellow"></div>
                                        </div>
                                    </div>
                                    <div class="progress-skill">
                                        <h2>Reading</h2>
                                        <div class="progress progress-mini">
                                            <div style="width: 80%;" class="progress-bar progress-green"></div>
                                        </div>
                                    </div>
                                    <div class="progress-skill">
                                        <h2>Writing</h2>
                                        <div class="progress progress-mini">
                                            <div style="width: 70%;" class="progress-bar progress-blue"></div>
                                        </div>
                                    </div>
                                    <div class="progress-skill">
                                        <h2>C#</h2>
                                        <div class="progress progress-mini">
                                            <div style="width: 60%;" class="progress-bar progress-red"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="comment-head lead-title">
                                        <h3>Comments</h3>
                                    </div>
                                </div>
                            </div>
                            @foreach ($class->comment as $comment)
                            <div class="row" style="min-height: 130px;">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="user-comment admin-comment">
                                        <img src="upload/users/{{$comment->user->image}}" alt="" />
                                        <div class="comment-details">
                                            <h4>{{$comment->user->name}} <span class="comment-replay">{{date('d-m-Y',strtotime($comment->created_at))}}</span></h4>
                                            <p>{!!$comment->comment!!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="lead-head lead-title">
                                        <h3>Leave A Comment</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="coment-area">
                                    <form id="comment" method="POST" action="{{route('comment_add', $class->id)}}" class="comment">
                                        {{ csrf_field() }}
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <textarea  name="comment" cols="30" rows="10" placeholder="Message"></textarea>
                                            </div>
                                            <div class="payment-adress comment-stn">
                                                <input type="submit" class="btn btn-primary waves-effect waves-light" value="Send">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<div class="single-pro-review-area mt-t-30 mg-b-15">
    <div class="container-fluid">
        <div class="row" style="padding-bottom: 30px">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="lead-head lead-title">
                    <h3>List Student</h3>
                </div>
            </div>
        </div>
        <div class="row" id="list_student">
            @foreach ($class->student as $student)
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <a href="{{route('student_info', [$student->id, str_slug($student->fullname)])}}">
                    
                <div class="student-inner-std res-mg-b-30">
                    <div class="student-img">
                        <img src="upload/students/{{$student->image}}" alt="" />
                    </div>
                    <div class="student-dtl">
                        <h2 style="height: 40px">{{$student->fullname}}</h2>
                        <p class="dp">{{$student->class->name}}</p>
                        <p class="dp-ag"><b>Student Code:</b> {{$student->id}}</p>
                    </div>
                </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
    
</div>
@endsection
@section('script')
@endsection