@extends('pages/teacher_index')
@section('title')
School Info
@endsection
@section('content')

<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <form role="search" class="sr-input-func">
                                    <input type="text" placeholder="Search..." class="search-int form-control">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Single pro tab review Start-->
<div class="single-pro-review-area mt-t-30 mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="profile-info-inner">
                    <div class="profile-img">
                        <img src="upload/schools/{{$school->image}}" alt="" />
                    </div>
                    <div class="profile-details-hr">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="address-hr">
                                    <p><b> {{$school->name}}</b></p>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="address-hr">
                                    <p>{{$school->address}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                <div class="address-hr">
                                    <p><b>Name</b><br />{{$school->name}}</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                <div class="address-hr tb-sm-res-d-n dps-tb-ntn">
                                    <p><b>Founding</b><br />{{$school->founding}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="address-hr">
                                    <p><b>Event</b> <br></p>
                                    <p>{!!$school->event!!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
        <div class="row">
            {!!$school->map!!}
        </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="product-payment-inner-st res-mg-t-30 analysis-progrebar-ctn">
                    <ul id="myTabedu1" class="tab-review-design">
                        <li><a href="#staff">Staff</a></li>
                        <li class="active"><a href="#class">Classes</a></li>
                        <li><a href="#report">Report</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content custom-product-edit">
                        <div class="product-tab-list tab-pane fade" id="staff">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="review-content-section">
                                        <div class="chat-discussion" style="height: auto">
                                            @foreach ($staffs as $staff)
                                            <div class="chat-message">
                                                <div class="profile-hdtc">
                                                    <img class="message-avatar" src="upload/staffs/{{$staff->image}}"
                                                        alt="">
                                                </div>
                                                <div class="message">
                                                    <a class="message-author" href="#"> {{$staff->name}}
                                                        ({{$staff->dob}})</a>
                                                    <span class="message-date">
                                                        <a class="btn btn-xs btn-success">{{$staff->position}}
                                                        </a></span>
                                                    <span class="message-content">
                                                        <b>Email:</b> {{$staff->email}} <a
                                                            class="btn btn-xs btn-primary"
                                                            href="mailto:{{$staff->email}}"><i
                                                                class="fa fa-envelope-o"></i> Message</a><br>
                                                        <b>Phone:</b> {{$staff->phone}} <a class="btn btn-xs btn-info"
                                                            href="tel:{{$staff->phone}}"><i
                                                                class="fa fa-phone"></i>Call</a><br>
                                                    </span>
                                                    <div class="m-t-md mg-t-10">
                                                        <b>Phụ trách:</b> {{$staff->charge_of}}
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-tab-list tab-pane fade  active in" id="class">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="review-content-section">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="skill-title">
                                                        <h2>Class list</h2>
                                                        <hr />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @foreach ($classes as $class)
                                        <div class="row" style="padding-bottom: 10px">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="classes-image">
                                                    <a
                                                        href="{{route('class_info', [ $class->id,$class->school->slug_name, $class->slug_name])}}"><img
                                                            src="upload/classes/{{$class->image}}" alt=""></a>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="courses-inner res-mg-b-30">
                                                    <div class="courses-title">
                                                        <h2>{{$class->name}}</h2>
                                                    </div>
                                                    <div class="courses-alaltic">
                                                        <span class="cr-ic-r"><span class="course-icon"><i
                                                                    class="fa fa-clock"></i></span> 1 Year</span>
                                                        <span class="cr-ic-r"><span class="course-icon"><i
                                                                    class="fa fa-heart"></i></span> 50</span>
                                                        <span class="cr-ic-r"><span class="course-icon"><i
                                                                    class="fa fa-dollar"></i></span> 500</span>
                                                    </div>
                                                    <div class="course-des">
                                                        <p><span><i class="fa fa-clock"></i></span> <b>Grade:</b> 6</p>
                                                        <p><span><i class="fa fa-clock"></i></span> <b>Program:</b>
                                                            {{$class->program->name}}</p>
                                                        <p><span><i class="fa fa-clock"></i></span> <b>Professor:</b>
                                                            Jane Doe</p>
                                                        <p><span><i class="fa fa-clock"></i></span> <b>Students:</b>
                                                            {{count($class->student)}}</p>
                                                    </div>
                                                    <div class="product-buttons">
                                                        <a class="btn btn-xs btn-primary"
                                                            href="{{route('class_info', [ $class->id,$class->school->slug_name, $class->slug_name])}}">Info</a>
                                                        {{-- <button type="button" class="button-default cart-btn">Info</button> --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-tab-list tab-pane fade" id="report">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="review-content-section">
                                        <form action="admin/schools/edit/{{$school->id}}" method="post"
                                            enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input name="fullname" type="text" class="form-control"
                                                            placeholder="fullname" value="{{$school->fullname}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="email" type="text" class="form-control"
                                                            placeholder="email" value="{{$school->email}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="address"
                                                            placeholder="Address" value="{{$school->address}}">
                                                    </div>
                                                    <div class="file-upload-inner ts-forms">
                                                        <div class="input prepend-big-btn">
                                                            <label class="icon-right" for="prepend-big-btn">
                                                                <i class="fa fa-download"></i>
                                                            </label>
                                                            <div class="file-button">
                                                                Browse
                                                                <input type="file" name="image"
                                                                    onchange="document.getElementById('prepend-big-btn').value = this.value;">
                                                            </div>
                                                            <input type="text" id="prepend-big-btn"
                                                                placeholder="no file selected"
                                                                value="{{$school->image}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="text" name="facebook" class="form-control"
                                                            placeholder="Facebook URL" value="{{$school->facebook}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="twitter" class="form-control"
                                                            placeholder="twitter URL" value="{{$school->twitter}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="dob" class="form-control"
                                                            placeholder="Date of Birth" value="{{$school->dob}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="mobile" class="form-control"
                                                            placeholder="Mobile no." value="{{$school->mobile}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="payment-adress mg-t-15">
                                                        <button type="submit"
                                                            class="btn btn-primary waves-effect waves-light mg-b-15">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection