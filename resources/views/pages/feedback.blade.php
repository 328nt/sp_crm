@extends('pages/teacher_index')
@section('title')
student Info
@endsection
@section('style')
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> --}}
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
<!-- Bootstrap CSS
		============================================ -->
<link rel="stylesheet" href="be/css/bootstrap.min.css">
<!-- modals CSS
		============================================ -->
<link rel="stylesheet" href="be/css/modals.css">
<!-- style CSS
		============================================ -->
<link rel="stylesheet" href="be/style.css">
@endsection
@section('content')

<div class="breadcome-area">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="breadcome-list single-page-breadcome">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="breadcome-heading">
                <form role="search" class="sr-input-func">
                  <input type="text" placeholder="Search..." class="search-int form-control">
                  <a href="#"><i class="fa fa-search"></i></a>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Single pro tab review Start-->

<div class="library-book-area mg-t-30">
  <div class="container-fluid">
    <div class="row">
      <div class="modal-bootstrap shadow-inner mg-tb-30 responsive-mg-b-0">
        <div class="modal-area-button">
          <a class="Warning Warning-color mg-b-10" href="#" data-toggle="modal"
            data-target="#WarningModalalert">feedback</a>
        </div>
      </div>
      <div id="WarningModalalert" class="modal modal-edu-general Customwidth-popup-WarningModal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-close-area modal-close-df">
              <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
            </div>
            <div class="modal-body">
              <span class="educate-icon educate-warning modal-check-pro information-icon-pro"></span>
              <h2>Feedback</h2>
              <form action="{{route('feedback_store')}}" method="post">
                {{ csrf_field() }}
                <div class="row">
                  <div class="form-group text-left col-md-12">
                    <label for="Name">Topic:</label>
                    <select class="form-control" name="topic" id="">
                      @foreach ($topics as $topic)
                      <option value="{{$topic->id}}">{{$topic->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group text-left col-md-12">
                    <label for="Address">Team:</label>
                    <select class="form-control" name="team_id" id="">
                      @foreach ($teams as $team)
                      <option value="{{$team->id}}">{{$team->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group text-left col-md-12" id="data_1">
                    <label for="deadline">Deadline (mm/dd/yyyy):</label>
                    <div class="input-group date">
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                      <input type="text" class="form-control" name="deadline" value="{{date('m/d/Y')}}">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group text-left col-md-12">
                    <label for="content">Content:</label>
                    <textarea name="content" id="event" cols="30" rows="5"></textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-5">
                    <div class="form-group text-left col-md-12">
                      <label for="content">Level Important:</label>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="bt-df-checkbox pull-left">
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="i-checks pull-left">
                            <label>
                              <input type="radio" value="1" name="serious">
                              <i class="educate-icon educate-star"></i>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="i-checks pull-left">
                            <label>
                              <input type="radio" value="2" name="serious">
                              <i class="educate-icon educate-star"></i>
                              <i class="educate-icon educate-star"></i>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="i-checks pull-left">
                            <label>
                              <input type="radio" value="3" name="serious">
                              <i class="educate-icon educate-star"></i>
                              <i class="educate-icon educate-star"></i>
                              <i class="educate-icon educate-star"></i>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                </div>
              </form>
            </div>
            <div class="modal-footer warning-md">
              <a data-dismiss="modal" href="#">Cancel</a>
              {{-- <a href="#">Process</a> --}}
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <div class="single-cards-item">
          <div class="single-product-image">
            <a href="#"><img src="be/img/product/profile-bg.jpg" alt=""></a>
          </div>
          <div class="single-product-text">
            <img src="upload/users/{{$user->image}}" alt="">
            <h4><a class="cards-hd-dn" href="#">{{$user->name}}</a></h4>
            <h5>Web Designer & Developer</h5>
            <p class="ctn-cards">Lorem ipsum dolor sit amet, this is a consectetur adipisicing elit</p>
            <a class="follow-cards" href="#">Follow</a>
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="cards-dtn">
                  <h3><span class="counter">199</span></h3>
                  <p>Articles</p>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="cards-dtn">
                  <h3><span class="counter">599</span></h3>
                  <p>Like</p>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="cards-dtn">
                  <h3><span class="counter">399</span></h3>
                  <p>Comment</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">

        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr>
              <th>Team</th>
              <th>Topic</th>
              <th>Deadline</th>
              <th>Important</th>
              <th>Status</th>
              <th>note</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($feedbacks as $feedback)
            <tr>
              <td>{{$feedback->team->name}}</td>
              <td>{{$feedback->topic->name}}</td>
              <td>{{$feedback->deadline}}</td>
              <td> @if ($feedback->serious == 1 )
                <label>
                  <i class="educate-icon educate-star"></i>
                </label>
                @elseif($feedback->serious == 2)
                <label>
                  <i class="educate-icon educate-star"></i>
                  <i class="educate-icon educate-star"></i>
                </label>
                @elseif($feedback->serious == 3)
                <label>
                  <i class="educate-icon educate-star"></i>
                  <i class="educate-icon educate-star"></i>
                  <i class="educate-icon educate-star"></i>
                </label>
                @endif </td>
              <td>{{$feedback->status->name}}</td>
              <td>
                @if ($feedback->note == null)

                @else

              <a class="Warning Warning-color mg-b-10" href="#" data-toggle="modal" data-target="#note-{{$feedback->id}}">Note</a>

                <div id="note-{{$feedback->id}}" class="modal modal-edu-general Customwidth-popup-WarningModal fade" role="dialog">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header header-color-modal bg-color-3">
                        <h4 class="modal-title">Review Feedback!</h4>
                        <div class="modal-close-area modal-close-df">
                          <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                        </div>
                      </div>
                      <div class="modal-body">
                        <span class="educate-icon educate-warning modal-check-pro information-icon-pro"></span>
                        <h2>Feedback info</h2>
                        <hr>
                        <div class="row">
                          <div class="col-md-6">
                            <h5 class="modal-title" id="modal-topic">{{$feedback->topic->name}}</h5>
                          </div>
                          <div class="col-md-6">
                            <h5 class="modal-title" id="modal-deadline">{{$feedback->deadline}}</h5>
                          </div>
                        </div>
                        <br>
                        <hr>
                        <input type="hidden" name="id" id="id">
                        <p id="content-feedback"></p>
                        <div class="row">
                          <div class="form-group text-left col-md-12">
                            <label for="content">Feedback:</label>
                            <h5 style=" border: 1px solid #bcc710; padding: 5px;" id="modal-content">
                              {{$feedback->content}}
                            </h5>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group text-left col-md-12">
                            <label for="note">Note:</label>
                            <textarea style=" border: 1px solid #2b10c7; padding: 5px;" name="note" id="modal-note"
                              cols="30" rows="5">
                                                      {!!$feedback->note!!}
                                                    </textarea>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer warning-md">
                        <a data-dismiss="modal" href="#">Cancel</a>
                        {{-- <a href="#">Process</a> --}}
                      </div>
                    </div>
                  </div>
                </div>
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>Team</th>
              <th>Topic</th>
              <th>Deadline</th>
              <th>Important</th>
              <th>Status</th>
              <th>note</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script2')
<script>
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<!-- jquery
		============================================ -->
<script src="be/js/vendor/jquery-1.12.4.min.js"></script>
<!-- bootstrap JS
        ============================================ -->
<script src="be/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
@endsection