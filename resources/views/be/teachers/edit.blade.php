@extends('be/layouts/index')
@section('title')
add new teacher
@endsection
@section('content')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <form role="search" class="sr-input-func">
                                    <input type="text" placeholder="Search..." class="search-int form-control">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="breadcome-menu">
                                <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                </li>
                                <li><span class="bread-blod">Add Professor</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Single pro tab review Start-->
<!-- Single pro tab review Start-->
<div class="single-pro-review-area mt-t-30 mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="product-payment-inner-st">
                    <ul id="myTabedu1" class="tab-review-design">
                        <li class="active"><a href="#account"> Account</a></li>
                        <li><a href="#info"> Information</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content custom-product-edit">
                        <div class="product-tab-list tab-pane fade active in" id="account">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="review-content-section">
                                        <form action="{{route('teacher_update', $teacher->id)}}" method="POST"
                                            enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                    {{ csrf_field() }}
                                                    <div class="devit-card-custom">
                                                        <div class="form-group">
                                                            <input name="name" type="text" class="form-control"
                                                                value="{{$teacher->name}}" placeholder="Full Name">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="email"
                                                                value="{{$teacher->email}}" placeholder="Email">
                                                        </div>
                                                        <div class="form-group">
                                                            <input name="mobile" type="number" class="form-control"
                                                                value="{{$teacher->mobile}}" placeholder="Phone">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">image</label>
                                                            <input class="form-control" name="image" type="file">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <img src="upload/users/{{$teacher->image}}" alt="">
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="product-tab-list tab-pane fade" id="info">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="review-content-section">
                                        <form action="{{route('teacher_update', $teacher->id)}}" method="POST"
                                            enctype="multipart/form-data">
                                            @if (isset($teacher->info_teacher))
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"
                                                            value="{{$teacher->info_teacher->starting_date}}"
                                                            name="starting_date" placeholder="starting_date">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"
                                                            value="{{$teacher->info_teacher->position}}" name="position"
                                                            placeholder="position">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"
                                                            value="{{$teacher->info_teacher->rate_sp}}" name="rate_sp"
                                                            placeholder="rate_sp">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"
                                                            value="{{$teacher->info_teacher->rate_center}}"
                                                            name="rate_center" placeholder="rate_center">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"
                                                            value="{{$teacher->info_teacher->personal_email}}"
                                                            name="personal_email" placeholder="personal_email">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"
                                                            value="{{$teacher->info_teacher->contact}}" name="contact"
                                                            placeholder="contact">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"
                                                            value="{{$teacher->info_teacher->nationality}}"
                                                            name="nationality" placeholder="nationality">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"
                                                            value="{{$teacher->info_teacher->passport}}" name="passport"
                                                            placeholder="passport">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <select name="school_id" class="form-control" id="">
                                                            @foreach ($schools as $school)
                                                            <option @if ($teacher->info_teacher->school_id == $school->id)
                                                                selected
                                                            @endif value="{{$school->id}}">{{$school->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"
                                                            value="{{$teacher->info_teacher->dob}}" name="dob"
                                                            placeholder="dob">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"
                                                            value="{{$teacher->info_teacher->address}}" name="address"
                                                            placeholder="address">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"
                                                            value="{{$teacher->info_teacher->state_health_insurance}}"
                                                            name="state_health_insurance"
                                                            placeholder="state_health_insurance">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"
                                                            value="{{$teacher->info_teacher->bussiness_visa}}"
                                                            name="bussiness_visa" placeholder="bussiness_visa">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"
                                                            value="{{$teacher->info_teacher->bv_duration}}"
                                                            name="bv_duration" placeholder="bv_duration">
                                                    </div>
                                                    <div class="form-group">
                                                        <textarea name="background" class="form-control"
                                                            placeholder="Background" id="" cols="30"
                                                            rows="10">{{$teacher->info_teacher->background}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                            @endif
                                        </form>
                                        <hr>
                                        @if (!isset($teacher->info_teacher))
                                        create
                                        <form action="{{route('info_store')}}" method="post">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <input type="text" name="user_id" value="{{$teacher->id}}"
                                                            hidden>
                                                        <input type="text" class="form-control" name="starting_date"
                                                            placeholder="starting_date">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="position"
                                                            placeholder="position">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="rate_sp"
                                                            placeholder="rate_sp">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="rate_center"
                                                            placeholder="rate_center">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="personal_email"
                                                            placeholder="personal_email">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="contact"
                                                            placeholder="contact">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="nationality"
                                                            placeholder="nationality">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="passport"
                                                            placeholder="passport">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <select name="school_id" class="form-control" id="">
                                                            @foreach ($schools as $school)
                                                            <option value="{{$school->id}}">{{$school->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="dob"
                                                            placeholder="dob">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="address"
                                                            placeholder="address">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"
                                                            name="state_health_insurance"
                                                            placeholder="state_health_insurance">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="bussiness_visa"
                                                            placeholder="bussinesss_visa">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="bv_duration"
                                                            placeholder="bv_duration">
                                                    </div>
                                                    <div class="form-group">
                                                        <textarea name="background" class="form-control"
                                                            placeholder="Background" id="" cols="30"
                                                            rows="10"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit"
                                                class="btn btn-primary waves-effect waves-light">Submit</button>
                                        </form>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')

<!-- metisMenu JS
		============================================ -->
<script src="be/js/metisMenu/metisMenu.min.js"></script>
<script src="be/js/metisMenu/metisMenu-active.js"></script>
<!-- morrisjs JS
		============================================ -->
<script src="be/js/sparkline/jquery.sparkline.min.js"></script>
<script src="be/js/sparkline/jquery.charts-sparkline.js"></script>
<!-- calendar JS
		============================================ -->
<script src="be/js/calendar/moment.min.js"></script>
<script src="be/js/calendar/fullcalendar.min.js"></script>
<script src="be/js/calendar/fullcalendar-active.js"></script>
<!-- maskedinput JS
		============================================ -->
<script src="be/js/jquery.maskedinput.min.js"></script>
<script src="be/js/masking-active.js"></script>
<!-- datepicker JS
		============================================ -->
<script src="be/js/datepicker/jquery-ui.min.js"></script>
<script src="be/js/datepicker/datepicker-active.js"></script>
<!-- form validate JS
		============================================ -->
<script src="be/js/form-validation/jquery.form.min.js"></script>
<script src="be/js/form-validation/jquery.validate.min.js"></script>
<script src="be/js/form-validation/form-active.js"></script>
<!-- dropzone JS
		============================================ -->
<script src="be/js/dropzone/dropzone.js"></script>
<!-- tab JS
		============================================ -->
<script src="be/js/tab.js"></script>
<!-- plugins JS
		============================================ -->
<script src="be/js/plugins.js"></script>
<!-- main JS
		============================================ -->
<script src="be/js/main.js"></script>
@endsection