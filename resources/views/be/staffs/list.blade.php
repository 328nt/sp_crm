@extends('be/layouts/index')
@section('title')
list Staff of staff
@endsection
@section('style')
@endsection
@section('content')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <form role="search" class="sr-input-func">
                                    <input type="text" placeholder="Search..." class="search-int form-control">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Static Table Start -->
<div class="data-table-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>List Staff</h1>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <table id="table" data-toggle="table" data-pagination="true" data-search="true"
                                data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true"
                                data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                data-cookie-id-table="saveId" data-show-export="false" data-click-to-select="true"
                                data-toolbar="#toolbar">
                                <thead>
                                    <tr>
                                        <th data-field="state" data-checkbox="true"></th>
                                        <th data-field="id">ID</th>
                                        <th data-field="name" data-editable="true">name</th>
                                        <th data-field="image" data-editable="false">Image</th>
                                        <th data-field="position" data-editable="false">position</th>
                                        <th data-field="Mob" data-editable="true">Mob</th>
                                        <th data-field="email" data-editable="true">email</th>
                                        <th data-field="report" data-editable="true">charge of</th>
                                        <th data-field="action">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($staffs as $staff)
                                    <tr>
                                        <td></td>
                                        <td>{{$staff->id}}</td>
                                        <td>{{$staff->name}}</td>
                                        <td><img src="upload/staffs/{{$staff->image}}" style=" height:100px;" alt=""></td>
                                        <td>{{$staff->position}} <br> {{$staff->school->name}}</td>
                                        <td>{{$staff->phone}}</td>
                                        <td>{{$staff->email}}</td>
                                        <td>{!!$staff->charge_of!!}</td>
                                        <td>
                                            <a href="{{route('staff_edit', $staff->id)}}" data-toggle="tooltip"
                                                title="Trash" class="pd-setting-ed"><i class="fa fa-pencil-square-o"
                                                    aria-hidden="true"></i> edit</a><br>
                                            <a href="{{route('staff_destroy', $staff->id)}}" data-toggle="tooltip"
                                                title="Trash" class="pd-setting-ed"><i class="fa fa-trash-o"
                                                    aria-hidden="true"></i> delete </a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Static Table End -->
@endsection