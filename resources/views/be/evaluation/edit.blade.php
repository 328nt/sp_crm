@extends('be/layouts/index')
@section('title')
Edit Evaluation
@endsection
@section('style')
<style>
    .solid {
        border: 1px solid #00486e;
        padding: 10px;
    }

    .b_ddd {
        border: 1px solid #ddd;
        padding: 5px;
    }

    .p_top10 {
        padding-top: 10px;
    }

    .p_top5 {
        padding-top: 5px;
    }

    .pd10 {
        padding: 30px;
    }
</style>
@endsection
@section('content')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <form role="search" class="sr-input-func">
                                    <input type="text" placeholder="Search..." class="search-int form-control">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="breadcome-menu">
                                <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                </li>
                                <li><span class="bread-blod">Accordion</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- accordion start-->
<div class="edu-accordion-area mg-b-15">
    <div class="container-fluid">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-md-offset-1 solid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="tab-content-details mg-b-30">
                        <h2>Evaluation</h2>
                    </div>
                    <div class="tab-content-details mg-b-30">
                        <h3>{{$evalu->schedule->teacher->fullname}} / {{$evalu->schedule->location->name}}
                        </h3>
                    </div>
                </div>
            </div>
            <form action="admin/evaluation/edit/{{$evalu->id}}" method="post">
                {{ csrf_field() }}
                <div class="row pd10">
                    <div class="admin-pro-accordion-wrap shadow-inner responsive-mg-b-30">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group-inner">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label class="login2 pull-right pull-right-pro">Time:</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                            <div class="data-custon-pick" id="data_1">
                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i
                                                            class="fa fa-calendar"></i></span>
                                                    <input type="text" name="time" class="form-control"
                                                        value="{{$evalu->test}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group-inner">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label class="login2 pull-right pull-right-pro">Lessoncontent:</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                            <input type="text" name="content" ta class="form-control"
                                                value="{{$evalu->content}}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group-inner">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label class="login2 pull-right pull-right-pro">Objective:</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                            <input type="text" name="objective" class="form-control"
                                                value="{{$evalu->objective}}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group-inner">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label class="login2 pull-right pull-right-pro">Lesson flow:</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                            <input type="text" name="lesson_flow" class="form-control"
                                                value="{{$evalu->lesson_flow}}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group-inner">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label class="login2 pull-right pull-right-pro">Strengths:</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                            <input type="text" name="strengths" class="form-control"
                                                value="{{$evalu->strengths}}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group-inner">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label class="login2 pull-right pull-right-pro">Areas for
                                                improvement:</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                            <input type="text" name="improvement" class="form-control"
                                                value="{{$evalu->improvement}}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-group edu-custon-design" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading accordion-head">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                            PART 1 - PRE-REQUISITES</a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse panel-ic collapse  in">
                                    <div class="panel-body admin-panel-content animated bounce">
                                        <div class="row">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <div class="i-checks pull-left">
                                                    <label>Teacher is professional and shows respect to all
                                                        learners.</label> <br>
                                                    <label>Teacher displays solid knowledge of the subject.</label>
                                                </div>
                                                <div class="i-checks pull-right">
                                                    <input type="checkbox" @if ($evalu->part1['part_1_1'] == 1)
                                                    checked
                                                    @endif
                                                    name="part_1_1" value="1">
                                                    <br>
                                                    <input type="checkbox" @if ($evalu->part1['part_1_2'] == 2)
                                                    checked
                                                    @endif
                                                    name="part_1_2" value="2">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Part2-->
                            <div class="panel panel-default">
                                <div class="panel-heading accordion-head">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                            PART 2 - CLASSROOM ENVIRONMENT</a>
                                    </h4>
                                </div>
                                <div id="collapse2" class="panel-collapse panel-ic collapse">
                                    <div class="panel-body admin-panel-content animated bounce">

                                        <div class="row p_top10 b_ddd">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <label class="login2 pull-left pull-left-pro">2A. Managing Classroom
                                                    Space, Materials & Procedures
                                                </label>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">2A.1. Space
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>
                                                            Teacher checks and/or ensures students' desks and chairs are
                                                            arranged for safe movement.</p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2a['p2a1']['basic']
                                                            == 1)
                                                            checked
                                                            @endif name="part_2a1_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher checks and/or ensures classroom
                                                            setup allows
                                                            students to have access to instruction with limited
                                                            distractions. </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2a['p2a1']['appro']
                                                            == 2)
                                                            checked
                                                            @endif name="part_2a1_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong> Teacher organizes and uses classroom
                                                            setup
                                                            to allow maximum engagement (T-S, S-S).</p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2a['p2a1']['compe']
                                                            == 3)
                                                            checked
                                                            @endif
                                                            name="part_2a1_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                                (x4):</strong>Teacher makes creative use of classroom
                                                            setup and space that highly engages students in a way that
                                                            is worth being replicated and systemized.</p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2a['p2a1']['outst']
                                                            == 4)
                                                            checked
                                                            @endif name="part_2a1_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">2A.2. Materials
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher provides a variety of materials.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2a['p2a2']['basic']
                                                            == 1)
                                                            checked
                                                            @endif name="part_2a2_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher provides visually appealing
                                                            materials.</p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2a['p2a2']['appro']
                                                            == 2)
                                                            checked
                                                            @endif name="part_2a2_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Teacher adequately exploits each presented
                                                            material (in terms of time allowance, class coverage,
                                                            content inquiry) to support learning goals and different
                                                            lesson stages. </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2a['p2a2']['compe']
                                                            == 3)
                                                            checked
                                                            @endif name="part_2a2_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                                (x4):</strong>Teacher makes creative use of teaching
                                                            aids that highly motivates and challenges students in a way
                                                            that is worth being replicated and systemized.</p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2a['p2a2']['outst']
                                                            == 4)
                                                            checked
                                                            @endif name="part_2a2_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">2A.3. Procedures
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher gives clear and orderly
                                                            instructions for transitions and other routines (e.g.
                                                            handling materials, greeting, break etc.)</p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2a['p2a3']['basic']
                                                            == 1)
                                                            checked
                                                            @endif name="part_2a3_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher executes transitions and other
                                                            routines smoothly with minimal loss of instructional time.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2a['p2a3']['appro']
                                                            == 2)
                                                            checked
                                                            @endif name="part_2a3_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Students contribute to the managing
                                                            groups, transitioning between activities, and handling
                                                            materials and supplies.</p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2a['p2a3']['compe']
                                                            == 3)
                                                            checked
                                                            @endif name="part_2a3_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <!--2B-->
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <label class="login2 pull-left pull-left-pro">2B. Managing Student
                                                    Behavior
                                                </label>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">2B.1. Setting &
                                                            Monitoring Rules/Expectations
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher clearly states and/or displays
                                                            classroom rules, expectations.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2b['p2b1']['basic']
                                                            == 1)
                                                            checked
                                                            @endif name="part_2b1_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher demonstrates continuous active
                                                            supervision across the classroom & activities.</p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2b['p2b1']['appro']
                                                            == 2)
                                                            checked
                                                            @endif name="part_2b1_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">2B.2. Responding
                                                            to & Fostering Student Behavior
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher acknowledges students' positive
                                                            behaviors.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2b['p2b2']['basic']
                                                            == 1)
                                                            checked
                                                            @endif name="part_2b2_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher frequently reminds students of
                                                            expected positive behaviors.</p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2b['p2b2']['appro']
                                                            == 2)
                                                            checked
                                                            @endif name="part_2b2_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Teacher positively and effectively handles
                                                            misbehaviors (e.g. redirecting undesired behaviors to
                                                            replacement ones, proximity control, time-out, follow-up
                                                            conversations) without hugely disrupting the class flow and
                                                            attention.</p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2b['p2b2']['compe']
                                                            == 3)
                                                            checked
                                                            @endif name="part_2b2_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                                (x4):</strong>At least 80% students follow classroom
                                                            procedures and always show appropriate behavior.</p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2b['p2b2']['outst']
                                                            == 4)
                                                            checked
                                                            @endif name="part_2b2_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <!--2c-->
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <label class="login2 pull-left pull-left-pro">2C. Creating an
                                                    Environment of Respect and Rapport
                                                </label>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">Respect & Rapport
                                                            & Caring
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher inquires about students' life
                                                            and/or interests beyond the class and school.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2c['p2c']['basic']
                                                            == 1)
                                                            checked
                                                            @endif name="part_2c_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher shows sensitivity to students'
                                                            inclinations. (E.g. not exhibiting biases against
                                                            mentalities, personalities, gender.)
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2c['p2c']['appro']
                                                            == 2)
                                                            checked
                                                            @endif name="part_2c_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Teacher demonstrates understanding of
                                                            students and their interests beyond the class and school.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2c['p2c']['compe']
                                                            == 3)
                                                            checked
                                                            @endif name="part_2c_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                                (x4):</strong>Most students naturally feel and show
                                                            caring and empathy with peers and teachers throughout the
                                                            class.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2c['p2c']['outst']
                                                            == 4)
                                                            checked
                                                            @endif name="part_2c_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <!--2D-->
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <label class="login2 pull-left pull-left-pro">2D. Creating a Culture for
                                                    Learning
                                                </label>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">2D.1. Effort &
                                                            Persistence
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher communicates that teacher won't
                                                            consider a lesson "finished" until most students achieve the
                                                            lesson objectives.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2d['p2d1']['basic']
                                                            == 1)
                                                            checked
                                                            @endif name="part_2d1_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher reinforces high expectations of
                                                            student effort. (E.g. taking a firm stand against inadequate
                                                            efforts and subpar performance relative to each student's
                                                            ability, except for health issues and other objective
                                                            reasons)
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2d['p2d1']['appro']
                                                            == 2)
                                                            checked
                                                            @endif name="part_2d1_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Most students achieve objectives of the
                                                            lesson and/or activities.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2d['p2d1']['compe']
                                                            == 3)
                                                            checked
                                                            @endif name="part_2d1_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                                (x4):</strong>Most students in class are intrinsically
                                                            driven to complete work of high quality relative to each
                                                            individual's ability.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2d['p2d1']['outst']
                                                            == 4)
                                                            checked
                                                            @endif name="part_2d1_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">2D.2. Passion to
                                                            Learn
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher shows interest (e.g. through body
                                                            gestures, voice, tone,...) in delivering the lesson.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2d['p2d2']['basic']
                                                            == 1)
                                                            checked
                                                            @endif name="part_2d2_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher explains the importance of
                                                            learning, subject and/or lesson content.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2d['p2d2']['appro']
                                                            == 2)
                                                            checked
                                                            @endif name="part_2d2_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Most students demonstrate a genuine desire
                                                            to understand the lesson rather than, for example, just
                                                            learn a procedure to getting the correct answer.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part2d['p2d2']['compe']
                                                            == 3)
                                                            checked
                                                            @endif name="part_2d2_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Part3-->
                            <div class="panel panel-default">
                                <div class="panel-heading accordion-head">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                            PART 3 - INSTRUCTION</a>
                                    </h4>
                                </div>
                                <div id="collapse3" class="panel-collapse panel-ic collapse">
                                    <div class="panel-body admin-panel-content animated bounce">

                                        <div class="row p_top10 b_ddd">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <label class="login2 pull-left pull-left-pro">3A. Communicating with
                                                    Students
                                                </label>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">3A.1. Setting
                                                            Objectives
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>"Teacher clearly states lesson objectives
                                                            verbally and/or in
                                                            written form at the beginning and/or throughout the lesson.
                                                            "
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a1']['basic']
                                                            == 1)
                                                            checked
                                                            @endif name="part_3a1_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher helps students understand lesson
                                                            objectives in
                                                            relation to previous and future learning.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a1']['appro']
                                                            == 2)
                                                            checked
                                                            @endif name="part_3a1_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">3A.2. Giving
                                                            Instruction
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher gives/demonstrates clear
                                                            instructions for each
                                                            teaching activity.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a2']['basic']
                                                            == 1)
                                                            checked
                                                            @endif name="part_3a2_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher effectively uses ICQs to check
                                                            students' understanding
                                                            of instructions.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a2']['appro']
                                                            == 2)
                                                            checked
                                                            @endif name="part_3a2_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Teacher gives detailed instructions of how
                                                            to achieve
                                                            satisfactory outcomes in each activity.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a2']['compe']
                                                            == 3)
                                                            checked
                                                            @endif name="part_3a2_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                                (x4):</strong>Most students implicitly understand and
                                                            clearly demonstrate
                                                            what standards are expected of them during each
                                                            activity/task.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a2']['outst']
                                                            == 4)
                                                            checked
                                                            @endif name="part_3a2_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">3A.3. Content
                                                            Delivery & Clarity
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher explains the content correctly.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a3']['basic']
                                                            == 1)
                                                            checked
                                                            @endif name="part_3a3_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher clearly answers students'
                                                            clarification questions.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a3']['appro']
                                                            == 2)
                                                            checked
                                                            @endif name="part_3a3_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Teacher uses written and verbal language
                                                            appropriate to
                                                            students' developmental age and background.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a3']['compe']
                                                            == 3)
                                                            checked
                                                            @endif name="part_3a3_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>"Outstanding
                                                                (x4):</strong>Teacher
                                                            explains content imaginatively. (E.g. using metaphors,
                                                            analogies, similes to
                                                            bring content to life with rich language.)
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a3']['outst']
                                                            == 4)
                                                            checked
                                                            @endif name="part_3a3_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">3A.4. Promoting
                                                            the Use of English
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher uses English mostly, only
                                                            resorting to Vietnamese when
                                                            necessary.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a4']['basic_1']
                                                            == 1)
                                                            checked
                                                            @endif name="part_3a4_1_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher provides correct examples (written
                                                            or verbal) of
                                                            phrases/sentences containing language feature.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a4']['appro_1']
                                                            == 2)
                                                            checked
                                                            @endif name="part_3a4_1_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Teacher consistently raises students'
                                                            awareness of stresses
                                                            and intonations in the English language.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a4']['compe_1']
                                                            == 3)
                                                            checked
                                                            @endif name="part_3a4_1_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>"Outstanding
                                                                (x4):</strong>Most
                                                            students confidently use English in class, with students'
                                                            total talking time in
                                                            English more than 50% of the lesson.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a4']['outst_1']
                                                            == 4)
                                                            checked
                                                            @endif name="part_3a4_1_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">3A.4. Promoting
                                                            the Use of English
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher encourages students to only use
                                                            English in class.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a4']['basic_2']
                                                            == 1)
                                                            checked
                                                            @endif name="part_3a4_2_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher provides useful basic language so
                                                            that students can
                                                            effectively communicate in English (S-S, S-T).
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a4']['appro_2']
                                                            == 2)
                                                            checked
                                                            @endif name="part_3a4_2_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Teacher uses activities with a logical
                                                            progression (before,
                                                            during, after each activity and between activities) to help
                                                            students develop
                                                            their abilities to use English.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a4']['compe_2']
                                                            == 3)
                                                            checked
                                                            @endif name="part_3a4_2_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher takes advantage of
                                                            impromptu exchanges (S-T, S-S) in class to empower students
                                                            to use English
                                                            more effectively and/or creatively.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3a['p3a4']['outst_2']
                                                            == 4)
                                                            checked
                                                            @endif name="part_3a4_2_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <!--2B-->
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <label class="login2 pull-left pull-left-pro">3B. Engaging Students in
                                                    Learning
                                                </label>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">3B.1. Student
                                                            Engagement
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher ensures students exhibit body
                                                            postures indicating that
                                                            they are paying attention (to teacher, other students,
                                                            instructional materials).
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3b['p3b1']['basic_1']
                                                            == 1)
                                                            checked
                                                            @endif name="part_3b1_1_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher uses suitable lesson pacing that
                                                            is neither dragged
                                                            out nor rushed.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3b['p3b1']['appro_1']
                                                            == 2)
                                                            checked
                                                            @endif name="part_3b1_1_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Most students actively work on most
                                                            assignments rather than
                                                            watching while their teacher "works".
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3b['p3b1']['compe_1']
                                                            == 3)
                                                            checked
                                                            @endif name="part_3b1_1_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                                (x4):</strong>Most
                                                            students are engaged in activities that require high-level
                                                            cognitive and/or
                                                            knowledge dimension with content and English (in accordance
                                                            with Bloom
                                                            taxonomy).
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3b['p3b1']['outst_1']
                                                            == 4)
                                                            checked
                                                            @endif name="part_3b1_1_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">3B.1. Student
                                                            Engagement
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher gives students an opportunity for
                                                            reflection to
                                                            consolidate their learning after an activity/lesson.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3b['p3b1']['basic_2']
                                                            == 1)
                                                            checked
                                                            @endif name="part_3b1_2_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher gives students opportunities to
                                                            explain their thought
                                                            process as part of completing a task.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3b['p3b1']['appro_2']
                                                            == 2)
                                                            checked
                                                            @endif name="part_3b1_2_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>As students explain their thought process,
                                                            teacher effectively
                                                            facilitates the thought process for struggling students.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3b['p3b1']['compe_2']
                                                            == 3)
                                                            checked
                                                            @endif name="part_3b1_2_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                                (x4):</strong>
                                                            students explain their thought process, teacher effectively
                                                            further
                                                            challenges/broadens their thinking and understanding.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3b['p3b1']['outst_2']
                                                            == 4)
                                                            checked
                                                            @endif name="part_3b1_2_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">3B.2. Scaffolding
                                                            & Differentiating
                                                            Instruction
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher uses a mix of student-grouping.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3b['p3b2']['basic']
                                                            == 1)
                                                            checked
                                                            @endif name="part_3b2_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher gives differentiating materials
                                                            and tasks for
                                                            different groups based on abilities, learning preferences,
                                                            personalities.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3b['p3b2']['appro']
                                                            == 2)
                                                            checked
                                                            @endif name="part_3b2_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Teacher effectively executes a variety of
                                                            scaffolding or
                                                            differentiation strategies to cater to the varied needs of
                                                            students. (E.g.
                                                            difficult tasks are simplified, slowed down, further
                                                            practiced or skipped, while
                                                            easy tasks are added with extra challenges)
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3b['p3b2']['compe']
                                                            == 3)
                                                            checked
                                                            @endif name="part_3b2_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                                (x4):</strong>Students take justifiable initiatives to
                                                            adapt the lesson.
                                                            E.g. (1) modifying a learning task to make it more
                                                            meaningful and relevant to
                                                            students' needs, (2) suggesting modifications to the
                                                            grouping patterns used, (3)
                                                            suggesting modifications or additions to the materials being
                                                            used, (4) choosing
                                                            to complete the task individually, in pairs, in groups or in
                                                            a different format:
                                                            poem vs essay etc.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3b['p3b2']['outst']
                                                            == 4)
                                                            checked
                                                            @endif name="part_3b2_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <!--3c-->
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <label class="login2 pull-left pull-left-pro">3C. Using Questioning and
                                                    Discussion
                                                    Techniques
                                                </label>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">3C.1. Low- &
                                                            High-level Questioning
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher gives concise and easy to
                                                            understand questions.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3c['p3c1']['basic_1']
                                                            == 1)
                                                            checked
                                                            @endif name="part_3c1_1_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher gives open-ended questions
                                                            (despite some low-level
                                                            questions) that offer multiple possible answers.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3c['p3c1']['appro_1']
                                                            == 2)
                                                            checked
                                                            @endif name="part_3c1_1_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Teacher incorporates students' questions
                                                            into the lesson,
                                                            encouraging other students to discuss the questions at hand.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3c['p3c1']['compe_1']
                                                            == 3)
                                                            checked
                                                            @endif name="part_3c1_1_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                                (x4):</strong>Most students effectively initiate
                                                            higher-order questions that
                                                            are appropriate to their cognitive developmental stage.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3c['p3c1']['outst_1']
                                                            == 4)
                                                            checked
                                                            @endif name="part_3c1_1_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">3C.1. Low- &
                                                            High-level Questioning
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher uses wait time.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3c['p3c1']['basic_2']
                                                            == 1)
                                                            checked
                                                            @endif name="part_3c1_2_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">3C.2. Discussion
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher creates opportunities for sharing
                                                            of ideas among the
                                                            whole class/groups.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3c['p3c2']['basic']
                                                            == 1)
                                                            checked
                                                            @endif name="part_3c2_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher allows students to connect their
                                                            personal experiences
                                                            with the subject at hand to generate interest and
                                                            discussion.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3c['p3c2']['appro']
                                                            == 2)
                                                            checked
                                                            @endif name="part_3c2_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Teacher establishes a real-world
                                                            connection between students
                                                            and the subject at hand to generate a class-level interest
                                                            and productive
                                                            discussions.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3c['p3c2']['compe']
                                                            == 3)
                                                            checked
                                                            @endif name="part_3c2_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                                (x4):</strong>Students actively invite comments from
                                                            classmates/ teacher
                                                            during discussion and challenge one another's thinking.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3c['p3c2']['outst']
                                                            == 4)
                                                            checked
                                                            @endif name="part_3c2_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <!--2D-->
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <label class="login2 pull-left pull-left-pro">3D. Using Assessment in
                                                    Instruction & Feedback
                                                </label>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">3D.1. Monitoring
                                                            of Student Learning
                                                            with Checks for Understanding
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher assesses assumed prior knowledge
                                                            of students at the
                                                            beginning of lesson. (E.g. using student scores or quick
                                                            check activities.)
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3d['p3d1']['basic']
                                                            == 1)
                                                            checked
                                                            @endif name="part_3d1_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher frequently uses formative
                                                            assessment techniques (e.g.
                                                            posing specifically created questions, CCQs) to elicit
                                                            information about student
                                                            understanding.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3d['p3d1']['appro']
                                                            == 2)
                                                            checked
                                                            @endif name="part_3d1_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Teacher effectively addresses any
                                                            misconceptions or
                                                            misunderstanding throughout the lesson for most students.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3d['p3d1']['compe']
                                                            == 3)
                                                            checked
                                                            @endif name="part_3d1_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                                (x4):</strong>Most students effectively monitor their
                                                            own understanding,
                                                            either on their own initiative or as a result of tasks set
                                                            by the teacher.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3d['p3d1']['outst']
                                                            == 4)
                                                            checked
                                                            @endif name="part_3d1_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">3D.2. Feedback to
                                                            Students
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher walks around classroom to monitor
                                                            learning and/or
                                                            participation.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3d['p3d2']['basic_1']
                                                            == 1)
                                                            checked
                                                            @endif name="part_3d2_1_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher supports struggling groups. (E.g.
                                                            providing clues,
                                                            facilitating the teamwork process.)
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3d['p3d2']['appro_1']
                                                            == 2)
                                                            checked
                                                            @endif name="part_3d2_1_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Teacher gives appropriate feedback to
                                                            class/group for level of
                                                            instruction. (E.g. task-level for teaching new materials;
                                                            process-level for
                                                            practice and application lessons)
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3d['p3d2']['compe_1']
                                                            == 3)
                                                            checked
                                                            @endif name="part_3d2_1_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                                (x4):</strong>Most students are aware of assessment
                                                            criteria and able to
                                                            assess themselves according to such criteria.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3d['p3d2']['outst_1']
                                                            == 4)
                                                            checked
                                                            @endif name="part_3d2_1_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row p_top10">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher shows attention to most students'
                                                            works (oral,
                                                            written).
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3d['p3d2']['basic_2']
                                                            == 1)
                                                            checked
                                                            @endif name="part_3d2_2_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher shows attention to most students'
                                                            works (oral,
                                                            written).
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3d['p3d2']['appro_2']
                                                            == 2)
                                                            checked
                                                            @endif name="part_3d2_2_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Teacher gives appropriate feedback to
                                                            class/group for level of
                                                            instruction. (E.g. task-level for teaching new materials;
                                                            process-level for
                                                            practice and application lessons)
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3d['p3d2']['compe_2']
                                                            == 3)
                                                            checked
                                                            @endif name="part_3d2_2_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                                (x4):</strong>Most students are aware of assessment
                                                            criteria and able to
                                                            assess themselves according to such criteria.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3d['p3d2']['outst_2']
                                                            == 4)
                                                            checked
                                                            @endif name="part_3d2_2_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row p_top10">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher acknowledges students'
                                                            sharing/answers.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3d['p3d2']['basic_3']
                                                            == 1)
                                                            checked
                                                            @endif name="part_3d2_3_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher uses appropriate correction
                                                            techniques (oral,
                                                            written).
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3d['p3d2']['appro_3']
                                                            == 2)
                                                            checked
                                                            @endif name="part_3d2_3_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Teacher effectively coaches students on
                                                            how to give
                                                            constructive feedback on their peers' work.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part3d['p3d2']['compe_3']
                                                            == 3)
                                                            checked
                                                            @endif name="part_3d2_3_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading accordion-head">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                                            Part 4 - Lesson Planning & Feedback</a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse panel-ic collapse">
                                    <div class="panel-body admin-panel-content animated bounce">
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                            <label class="login2 pull-left pull-left-pro">4A. Lesson Planning</label>
                                        </div>
                                        <div class="form-group-inner">
                                            <div class="row">
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <label class="login2 pull-left pull-left-pro">4A.1. Objectives
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                    <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                            (x1):</strong>Lesson objective is aligned with level
                                                        outcomes.
                                                    </p>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                    <div class="i-checks pull-right">
                                                        <input type="checkbox" @if ($evalu->part4a['p4a1']['basic']
                                                        == 1)
                                                        checked
                                                        @endif name="part_4a1_basic" value="1">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                    <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                            (x2):</strong>Objectives are performance-based in terms of
                                                        what students can do
                                                        (e.g. using Bloom verbs).
                                                    </p>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                    <div class="i-checks pull-right">
                                                        <input type="checkbox" @if ($evalu->part4a['p4a1']['appro']
                                                        == 2)
                                                        checked
                                                        @endif name="part_4a1_appro" value="2">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                    <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                            (x3):</strong>Teacher anticipates problems and plans
                                                        solutions. (E.g. how to
                                                        deal with language questions, early finishers, how to scale
                                                        up/down activities,
                                                        possible student misconceptions and how to address those.)
                                                    </p>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                    <div class="i-checks pull-right">
                                                        <input type="checkbox" @if ($evalu->part4a['p4a1']['compe']
                                                        == 3)
                                                        checked
                                                        @endif name="part_4a1_compe" value="3">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <label class="login2 pull-left pull-left-pro">4A.2. Sequence &
                                                        Balance
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                    <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                            (x1):</strong>For each task/activity, lesson plan provides
                                                        task description,
                                                        task aim, procedure, and success criteria.
                                                    </p>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                    <div class="i-checks pull-right">
                                                        <input type="checkbox" @if ($evalu->part4a['p4a2']['basic_1']
                                                        == 1)
                                                        checked
                                                        @endif name="part_4a2_1_basic" value="1">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                    <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                            (x2):</strong>Lesson is logically staged in order for each
                                                        learning activity to
                                                        contribute towards achieving the lesson outcomes.
                                                    </p>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                    <div class="i-checks pull-right">
                                                        <input type="checkbox" @if ($evalu->part4a['p4a2']['appro_1']
                                                        == 2)
                                                        checked
                                                        @endif name="part_4a2_1_appro" value="2">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                    <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                            (x3):</strong>Lesson plan shows diagnostic, formative, or
                                                        summative assessments
                                                        to monitor learning of students of varied abilities throughout
                                                        the lesson.
                                                    </p>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                    <div class="i-checks pull-right">
                                                        <input type="checkbox" @if ($evalu->part4a['p4a2']['compe_1']
                                                        == 3)
                                                        checked
                                                        @endif name="part_4a2_1_compe" value="3">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                    <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                            (x4):</strong>Lesson plan shows an innovative approach in
                                                        integrating language
                                                        skills with communication and/or collaboration that is worth
                                                        being replicated and
                                                        systemized.
                                                    </p>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                    <div class="i-checks pull-right">
                                                        <input type="checkbox" @if ($evalu->part4a['p4a2']['outst_1']
                                                        == 4)
                                                        checked
                                                        @endif name="part_4a2_1_outst" value="4">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <label class="login2 pull-left pull-left-pro">4A.2. Sequence &
                                                        Balance
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                    <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                            (x2):</strong>There is appropriate time allocation to
                                                        learning activities and transitions.</p>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                    <div class="i-checks pull-right">
                                                        <input type="checkbox" @if ($evalu->part4a['p4a2']['appro_2']
                                                        == 2)
                                                        checked
                                                        @endif name="part_4a2_2_appro" value="2">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                    <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                            (x3):</strong>Lesson plan shows differentiation principles
                                                        to facilitate learning for multiple learner types.</p>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                    <div class="i-checks pull-right">
                                                        <input type="checkbox" @if ($evalu->part4a['p4a2']['compe_2']
                                                        == 3)
                                                        checked
                                                        @endif name="part_4a2_2_compe" value="3">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                    <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                            (x4):</strong>Lesson plan shows an innovative approach in
                                                        integrating language skills with creativity and/or critical
                                                        thinking that is worth being replicated and systemized.</p>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                    <div class="i-checks pull-right">
                                                        <input type="checkbox" @if ($evalu->part4a['p4a2']['outst_2']
                                                        == 4)
                                                        checked
                                                        @endif name="part_4a2_2_outst" value="4">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <label class="login2 pull-left pull-left-pro">4B. Knowledge of Content &
                                                    Pedagogy
                                                </label>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">4B. Knowledge of
                                                            Content & Pedagogy
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Lesson plan shows that teacher can
                                                            identify important concepts
                                                            and skills of the discipline.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part4b['p4b']['basic']
                                                            == 1)
                                                            checked
                                                            @endif name="part_4b_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Lesson plan shows that instructional
                                                            strategies are suitable
                                                            to lesson content.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part4b['p4b']['appro']
                                                            == 2)
                                                            checked
                                                            @endif name="part_4b_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Lesson plan shows that instructional
                                                            strategies are entirely
                                                            suitable to students' developmental stage.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part4b['p4b']['compe']
                                                            == 3)
                                                            checked
                                                            @endif name="part_4b_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                                (x4):</strong>Lesson
                                                            plan shows an innovative approach to integrate intra- and
                                                            inter-disciplinary
                                                            content that is worth being replicated and systemized.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part4b['p4b']['outst']
                                                            == 4)
                                                            checked
                                                            @endif name="part_4b_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <label class="login2 pull-left pull-left-pro">4C. Reflection and
                                                    Receiving Feedback
                                                </label>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <label class="login2 pull-left pull-left-pro">4C. Reflection and
                                                            Receiving Feedback
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Basic
                                                                (x1):</strong>Teacher actively participates in
                                                            post-observation discussion.
                                                            (E.g. by listening to comments, asking questions to enhance
                                                            understanding,
                                                            discussing and taking notes if necessary.)
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part4c['p4c']['basic']
                                                            == 1)
                                                            checked
                                                            @endif name="part_4c_basic" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Approaching
                                                                (x2):</strong>Teacher can identify strengths and
                                                            weaknesses of the observed
                                                            lesson during post-observation meeting. (E.g. teacher
                                                            reflects on evidence of
                                                            students' learning by referring to some specific classroom
                                                            situation etc.)
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part4c['p4c']['appro']
                                                            == 2)
                                                            checked
                                                            @endif name="part_4c_appro" value="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Competent
                                                                (x3):</strong>Teacher can analyze what made areas of the
                                                            lesson effective or
                                                            less effective.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part4c['p4c']['compe']
                                                            == 3)
                                                            checked
                                                            @endif name="part_4c_compe" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                                        <p class="login2 pull-left pull-left-pro"><strong>Outstanding
                                                                (x4):</strong>There is
                                                            evidence of effective reflection on feedback, as teacher
                                                            establishes sound and
                                                            concrete learning points that he/she can apply in future
                                                            teaching.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                        <div class="i-checks pull-right">
                                                            <input type="checkbox" @if ($evalu->part4c['p4c']['outst']
                                                            == 4)
                                                            checked
                                                            @endif name="part_4c_outst" value="4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row pull-right">
                            <input type="submit" value="save">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<!-- icheck JS
		============================================ -->
<script src="be/js/icheck/icheck.min.js"></script>
<script src="be/js/icheck/icheck-active.js"></script>
<script src="be/js/datapicker/bootstrap-datepicker.js"></script>
<script src="be/js/datapicker/datepicker-active.js"></script>
@endsection