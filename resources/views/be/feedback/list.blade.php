@extends('be/layouts/index')
@section('title')
list School
@endsection
@section('style')
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> --}}
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
<!-- Bootstrap CSS
		============================================ -->
<link rel="stylesheet" href="be/css/bootstrap.min.css">
<!-- modals CSS
		============================================ -->
<link rel="stylesheet" href="be/css/modals.css">
<!-- style CSS
		============================================ -->
<link rel="stylesheet" href="be/style.css">
@endsection
@section('content')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <form role="search" class="sr-input-func">
                                    <input type="text" placeholder="Search..." class="search-int form-control">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="breadcome-menu">
                                <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                </li>
                                <li><span class="bread-blod">Data Table</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Static Table Start -->
<div class="data-table-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>List teacher</h1>
                        </div>
                    </div>

                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Team</th>
                                <th>Topic</th>
                                <th>Deadline</th>
                                <th>Important</th>
                                <th>Status</th>
                                <th>Info</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($feedbacks as $feedback)
                            <tr @if ($feedback->status_id == 1)
                                style="background-color:#00ff0836"
                                @elseif($feedback->status_id == 2)
                                style="background-color:#ff000030"
                                @elseif($feedback->status_id == 3)
                                style="background-color:#ccff0036"
                                @endif>
                                <td>{{$feedback->id}}</td>
                                <td>{{$feedback->team->name}}</td>
                                <td>{{$feedback->topic->name}}</td>
                                <td>{{$feedback->deadline}}</td>
                                <td> @if ($feedback->serious == 1 )
                                    <label>
                                        <i class="educate-icon educate-star"></i>
                                    </label>
                                    @elseif($feedback->serious == 2)
                                    <label>
                                        <i class="educate-icon educate-star"></i>
                                        <i class="educate-icon educate-star"></i>
                                    </label>
                                    @elseif($feedback->serious == 3)
                                    <label>
                                        <i class="educate-icon educate-star"></i>
                                        <i class="educate-icon educate-star"></i>
                                        <i class="educate-icon educate-star"></i>
                                    </label>
                                    @endif </td>
                                <td>{{$feedback->status->name}}</td>
                                <td>

                                    <a class="Warning Warning-color mg-b-10" href="#" data-toggle="modal"
                                        data-target="#feedback" data-id="{{ $feedback->id }}"
                                        data-deadline="{{$feedback->deadline}}" data-topic="{{$feedback->topic->name}}"
                                        data-content="{{ $feedback->content }}"
                                        data-note="{{ $feedback->note }}"
                                        data-status="{{ $feedback->status_id }}">feedback</a>

                                    <div id="feedback"
                                        class="modal modal-edu-general Customwidth-popup-WarningModal fade"
                                        role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-close-area modal-close-df">
                                                    <a class="close" data-dismiss="modal" href="#"><i
                                                            class="fa fa-close"></i></a>
                                                </div>
                                                <div class="modal-body">
                                                    <span
                                                        class="educate-icon educate-warning modal-check-pro information-icon-pro"></span>
                                                    <h2>Feedback</h2>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <h5 class="modal-title" id="modal-topic">New message</h5>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <h5 class="modal-title" id="modal-deadline">New message</h5>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <hr>
                                                    <form action="{{route('feedback_update')}}" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" id="id">
                                                        <p id="content-feedback"></p>
                                                        <div class="row">
                                                            <div class="form-group text-left col-md-12">
                                                                <label for="content">Feedback:</label>
                                                                <h5 style=" border: 1px solid #bcc710; padding: 5px;"
                                                                    id="modal-content">
                                                                    content
                                                                </h5>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group text-left col-md-12">
                                                                <label for="note">Note:</label>
                                                                <textarea name="note" id="modal-note" cols="30"
                                                                    rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group text-left col-md-12">
                                                                <label for="Name">Status:</label>
                                                                <select class="form-control" name="status_id" id="modal-status">
                                                                    @foreach ($statuses as $status)
                                                                    <option value="{{$status->id}}">{{$status->name}}
                                                                    </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <button type="submit"
                                                                class="btn btn-primary waves-effect waves-light">Submit</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer warning-md">
                                                    <a data-dismiss="modal" href="#">Cancel</a>
                                                    {{-- <a href="#">Process</a> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>id</th>
                                <th>Team</th>
                                <th>Topic</th>
                                <th>Deadline</th>
                                <th>Important</th>
                                <th>Status</th>
                                <th>Info</th>
                            </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script2')
<script>
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<script>
    $('#feedback').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var id = button.data('id') // Extract info from data-* attributes
  var content = button.data('content') // Extract info from data-* attributes
  var deadline = button.data('deadline') // Extract info from data-* attributes
  var topic = button.data('topic') // Extract info from data-* attributes
  var note = button.data('note') // Extract info from data-* attributes
  var status = button.data('status') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
        modal.find('#id').val(id)
        modal.find('#modal-note').val(button.data('modal-note'))
        modal.find('#modal-status').val(button.data('modal-status'))
        
        
        $('#feedback form select[name="status_id"]').val(status);
    modal.find('.modal-title').text('New message to ' + id)
    modal.find('#modal-topic').text('Topic: ' + topic)
    modal.find('#modal-deadline').text('deadline: ' + deadline)
    modal.find('#modal-content').text(content)
    modal.find('.modal-body textarea').val(note)
})
</script>
<!-- jquery
		============================================ -->
<script src="be/js/vendor/jquery-1.12.4.min.js"></script>
<!-- bootstrap JS
        ============================================ -->
<script src="be/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
@endsection