@extends('be/layouts/index')
@section('title')
List Student
@endsection
@section('style')
@endsection
@section('content')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <form role="search" class="sr-input-func">
                                    <input type="text" placeholder="Search..." class="search-int form-control">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Static Table Start -->
<div class="data-table-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>List Student</h1>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <table id="table" data-toggle="table" data-pagination="true" data-search="true"
                                data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true"
                                data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                data-cookie-id-table="saveId" data-show-export="false" data-click-to-select="true"
                                data-toolbar="#toolbar">
                                <thead>
                                    <tr>
                                        <th data-field="state" data-checkbox="true"></th>
                                        <th data-field="id">ID</th>
                                        <th data-field="school" data-editable="true">school</th>
                                        <th data-field="class" data-editable="true">class</th>
                                        <th data-field="fullname" data-editable="false">fullname</th>
                                        <th data-field="image" data-editable="false">image</th>
                                        <th data-field="dob" data-editable="true">dob</th>
                                        <th data-field="parent_info" data-editable="false">parent_info</th>
                                        <th data-field="action">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($students as $student)
                                    <tr>
                                        <td></td>
                                        <td>{{$student->id}}</td>
                                        <td>{{$student->class->school->name}}</td>
                                        <td>{{$student->class->name}}</td>
                                        <td>{{$student->fullname}}</td>
                                        <td><img src="upload/students/{{$student->image}}" style="height:100px"  alt=""></td>
                                        <td>{{$student->dob}}</td>
                                        <td>{{$student->dad_name}} / {{$student->dad_phone}} <br>
                                            {{$student->mom_name}} / {{$student->mom_phone}}</td>
                                        <td>
                                            <a href="{{route('student_edit', $student->id)}}" data-toggle="tooltip"
                                                title="Trash" class="pd-setting-ed"><i class="fa fa-pencil-square-o"
                                                    aria-hidden="true"></i> edit</a><br>
                                            <a href="{{route('student_destroy', $student->id)}}" data-toggle="tooltip"
                                                title="Trash" class="pd-setting-ed"><i class="fa fa-trash-o"
                                                    aria-hidden="true"></i> del </a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Static Table End -->
@endsection