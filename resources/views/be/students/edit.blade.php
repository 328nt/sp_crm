@extends('be/layouts/index')
@section('title')
edit Student
@endsection
@section('content')

<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <form role="search" class="sr-input-func">
                                    <input type="text" placeholder="Search..." class="search-int form-control">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Single pro tab review Start-->
<div class="single-pro-review-area mt-t-30 mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="product-payment-inner-st">
                    <ul id="myTabedu1" class="tab-review-design">
                        <li class="active"><a href="#description"> edit Student</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content custom-product-edit">
                        <div class="product-tab-list tab-pane fade active in" id="description">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="review-content-section">
                                        <div id="dropzone1" class="pro-ad">
                                            <form action="{{route('student_update', $student->id)}}" method="POST"
                                                enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <select name="school_id" class="form-control">
                                                            <option value="" selected="" hidden disabled="">
                                                                Select School</option>
                                                            @foreach ($schools as $school)
                                                            <option @if ($student->school_id == $school->id)
                                                                selected
                                                            @endif value="{{$school->id}}">{{$school->name}}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <select name="unit_class_id" class="form-control">
                                                            <option value="" selected="" hidden disabled="">
                                                                Select Class</option>
                                                            @foreach ($classes as $class)
                                                            <option @if ($student->unit_class_id == $class->id)
                                                                selected
                                                            @endif  value="{{$class->id}}">{{$class->name}}/ {{$class->school->name}}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="fullname" type="text" class="form-control"
                                                            placeholder="fullname *" value="{{$student->fullname}}">
                                                    </div>
                                                    <div class="form-group data-custon-pick" id="data_1">
                                                        <label>Date of Birth (mm/dd/yyyy)</label>
                                                        <div class="input-group date">
                                                            <span class="input-group-addon"><i
                                                                    class="fa fa-calendar"></i></span>
                                                            <input type="text" class="form-control" name="dob"
                                                                value="{{$student->dob}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">image</label>
                                                        <input class="form-control" name="image" type="file">
                                                        <img src="upload/students/{{$student->image}}" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <input name="dad_name" type="text" class="form-control"
                                                            placeholder="dad_name *" value="{{$student->dad_name}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="dad_phone" type="number" class="form-control"
                                                            placeholder="dad_phone *" value="{{$student->dad_phone}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="dad_email" type="text" class="form-control"
                                                            placeholder="dad_email *" value="{{$student->dad_email}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="mom_name" type="text" class="form-control"
                                                            placeholder="mom_name *" value="{{$student->mom_name}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="mom_phone" type="number" class="form-control"
                                                            placeholder="mom_phone *" value="{{$student->mom_phone}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="mom_email" type="text" class="form-control"
                                                            placeholder="mom_email *" value="{{$student->mom_email}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="payment-adress">
                                                        <button type="submit"
                                                            class="btn btn-primary waves-effect waves-light">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<!-- metisMenu JS
		============================================ -->
<script src="be/js/metisMenu/metisMenu.min.js"></script>
<script src="be/js/metisMenu/metisMenu-active.js"></script>
<!-- morrisjs JS
		============================================ -->
<script src="be/js/sparkline/jquery.sparkline.min.js"></script>
<script src="be/js/sparkline/jquery.charts-sparkline.js"></script>
<!-- calendar JS
		============================================ -->
<script src="be/js/calendar/moment.min.js"></script>
<script src="be/js/calendar/fullcalendar.min.js"></script>
<script src="be/js/calendar/fullcalendar-active.js"></script>
<!-- maskedinput JS
		============================================ -->
<script src="be/js/jquery.maskedinput.min.js"></script>
<script src="be/js/masking-active.js"></script>
<!-- datepicker JS
		============================================ -->
<script src="be/js/datepicker/jquery-ui.min.js"></script>
<script src="be/js/datepicker/datepicker-active.js"></script>
<!-- form validate JS
		============================================ -->
<script src="be/js/form-validation/jquery.form.min.js"></script>
<script src="be/js/form-validation/jquery.validate.min.js"></script>
<script src="be/js/form-validation/form-active.js"></script>
<!-- dropzone JS
		============================================ -->
<script src="be/js/dropzone/dropzone.js"></script>
<!-- tab JS
		============================================ -->
<script src="be/js/tab.js"></script>
<!-- plugins JS
		============================================ -->
<script src="be/js/plugins.js"></script>
<!-- main JS
		============================================ -->
<script src="be/js/main.js"></script>
@endsection