<div class="left-sidebar-pro">
    <nav id="sidebar" class="">
        <div class="sidebar-header">
            <a href="index.html"><img class="main-logo" src="be/img/logo-w.png" alt="" /></a>
            <strong><a href="index.html"><img src="be/img/logo-w.png" alt="" /></a></strong>
        </div>
        <div class="left-custom-menu-adp-wrap comment-scrollbar">
            <nav class="sidebar-nav left-sidebar-menu-pro">
                <ul class="metismenu" id="menu1">
                    <li>
                        <a title="Landing Page" href="/admin/dashboard" aria-expanded="false"><span
                                class="educate-icon educate-event icon-wrap sub-icon-mg" aria-hidden="true"></span>
                            <span class="mini-click-non">Dashboard</span></a>
                    </li>
                    <li class="{{ request()->route()->named('school*') ? 'active' : '' }} {{ request()->route()->named('monthly_rp*') ? 'active' : '' }}">
                        <a class="has-arrow" href="all-professors.html" aria-expanded="false"><span
                                class="educate-icon educate-professor icon-wrap"></span> <span
                                class="mini-click-non">Schools</span></a>
                        <ul class="submenu-angle" aria-expanded="false">
                            <li><a title="All Professors" href="{{route('schools_show')}}">
                                    <span class="mini-sub-pro">All Schools</span></a></li>
                            <li><a title="Add Professor" href="{{route('schools_create')}}">
                                    <span class="mini-sub-pro">Add Schools</span></a></li>
                        </ul>
                        <ul class="submenu-angle" aria-expanded="false">
                            <li><a title="All Professors" href="{{route('monthly_rp_show')}}">
                                    <span class="mini-sub-pro">All report</span></a></li>
                            <li><a title="Add Professor" href="{{route('monthly_rp_create')}}">
                                    <span class="mini-sub-pro">Add report</span></a></li>
                        </ul>
                    </li>
                    <li class="{{ request()->route()->named('program*') ? 'active' : '' }}">
                        <a class="has-arrow" href="all-professors.html" aria-expanded="false"><span
                                class="educate-icon educate-professor icon-wrap"></span> <span
                                class="mini-click-non">programs</span></a>
                        <ul class="submenu-angle" aria-expanded="false">
                            <li><a title="All Professors" href="{{route('programs_show')}}">
                                    <span class="mini-sub-pro">All programs</span></a></li>
                            <li><a title="Add Professor" href="{{route('programs_create')}}">
                                    <span class="mini-sub-pro">Add programs</span></a></li>
                        </ul>
                    </li>
                    <li class="{{ request()->route()->named('staff*') ? 'active' : '' }}">
                        <a class="has-arrow" href="all-professors.html" aria-expanded="false"><span
                                class="educate-icon educate-professor icon-wrap"></span> <span
                                class="mini-click-non">Staffs School</span></a>
                        <ul class="submenu-angle" aria-expanded="false">
                            <li><a title="All Professors" href="{{route('staffs_show')}}">
                                    <span class="mini-sub-pro">All Staffs</span></a></li>
                            <li><a title="Add Professor" href="{{route('staffs_create')}}">
                                    <span class="mini-sub-pro">Add Staffs</span></a></li>
                        </ul>
                    </li>
                    <li class="{{ request()->route()->named('class*') ? 'active' : '' }}">
                        <a class="has-arrow" href="all-professors.html" aria-expanded="false"><span
                                class="educate-icon educate-professor icon-wrap"></span> <span
                                class="mini-click-non">Class</span></a>
                        <ul class="submenu-angle" aria-expanded="false">
                            <li><a title="All Professors" href="{{route('class_show')}}">
                                    <span class="mini-sub-pro">All class</span></a></li>
                            <li><a title="Add Professor" href="{{route('class_create')}}">
                                    <span class="mini-sub-pro">Add class</span></a></li>
                        </ul>
                    </li>
                    <li class="{{ request()->route()->named('student*') ? 'active' : '' }}">
                        <a class="has-arrow" href="all-students.html" aria-expanded="false"><span
                                class="educate-icon educate-student icon-wrap"></span> <span
                                class="mini-click-non">Students</span></a>
                        <ul class="submenu-angle" aria-expanded="false">
                            <li><a title="All Professors" href="{{route('students_show')}}">
                                    <span class="mini-sub-pro">All Students</span></a></li>
                            <li><a title="Add Professor" href="{{route('student_create')}}">
                                    <span class="mini-sub-pro">Add student</span></a></li>
                        </ul>
                    </li>
                    <li class="{{  request()->route()->named('teacher*') ? 'active' : '' }}">
                        <a class="has-arrow" href="all-professors.html" aria-expanded="false"><span
                                class="educate-icon educate-professor icon-wrap"></span> <span
                                class="mini-click-non">Teachers</span></a>
                        <ul class="submenu-angle" aria-expanded="false">
                            <li><a title="All Professors" href="{{route('teachers_show')}}">
                                    <span class="mini-sub-pro">All teachers</span></a></li>
                            <li><a title="Add Professor" href="{{route('teacher_create')}}">
                                    <span class="mini-sub-pro">Add teacher</span></a></li>
                        </ul>
                    </li>
                    <li class="{{ request()->is('admin/users*') ? 'active' : '' }}">
                        <a class="has-arrow" href="all-students.html" aria-expanded="false"><span
                                class="educate-icon educate-student icon-wrap"></span> <span
                                class="mini-click-non">Admin</span></a>
                        <ul class="submenu-angle" aria-expanded="false">
                            <li><a title="All Students" href="admin/users/list"><span class="mini-sub-pro">All
                                        Admins</span></a></li>
                            <li><a title="Add Students" href="admin/users/add"><span class="mini-sub-pro">Add
                                        Admin</span></a></li>
                        </ul>
                    </li>
                    <li class="{{ request()->is('admin/schedules*') ? 'active' : '' }}">
                        <a class="has-arrow" href="all-students.html" aria-expanded="false"><span
                                class="educate-icon educate-student icon-wrap"></span> <span
                                class="mini-click-non">Schedules</span></a>
                        <ul class="submenu-angle" aria-expanded="false">
                            <li><a title="All Students" href="admin/schedules/list"><span class="mini-sub-pro">All
                                        Schedules</span></a></li>
                            <li><a title="Add Students" href="admin/schedules/add"><span class="mini-sub-pro">Add
                                        Schedules</span></a></li>
                        </ul>
                    </li>
                    
                    <li class="{{  request()->route()->named('feedbacks_show*') ? 'active' : '' }}">
                        <a title="Landing Page" href="{{route('feedbacks_show')}}" aria-expanded="false"><span
                                class="educate-icon educate-event icon-wrap sub-icon-mg" aria-hidden="true"></span>
                            <span class="mini-click-non">Feedback</span></a>
                    </li>
                </ul>
            </nav>
        </div>
    </nav>
</div>