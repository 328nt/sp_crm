<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('events', 'EventCalendarController@index');
Route::get('datatable', 'EventCalendarController@datatable');

Route::get('/on', function () {
    echo "ok";
})->middleware('test');
Route::get('/off', function () {
    echo "failed";
})->name('off');
Route::get('layout', function () {
    return view('layout');
});
Route::get('/manager/login', 'PagesController@getloginmanager')->name('get_login');
Route::post('/manager/login', 'PagesController@postloginmanager')->name('post_login');
Route::get('manager/logout', 'PagesController@logoutmanager')->name('manager_logout');

Route::group(['prefix' => 'manager', 'middleware'=> [ 'is_teacher']], function () {
    
    Route::get('teacher_info', 'PagesController@teacher_info')->name('teacher_info');
    Route::get('select_school', 'PagesController@select_school')->name('select_school');
    Route::post('school_selected', 'PagesController@school_selected')->name('school_selected');
    Route::post('add_new_school', 'CommentClassController@add_new_school')->name('add_new_school');
    Route::get('school/{id}/{slug_name}', 'PagesController@school_info')->name('school_info');
    Route::get('class/{id}/{school_slug}/{slug_name}', 'PagesController@class_info')->name('class_info');
    Route::get('student/info/{id}/{slug_name}', 'PagesController@student_info')->name('student_info');
    Route::post('class_comment/{id}', 'CommentClassController@store')->name('comment_add');
    Route::post('student/score/{id}', 'PagesController@score_student')->name('score_student');
    Route::post('student/test_score/{id}', 'PagesController@student_test')->name('student_test');
    Route::post('add', 'EventCalendarController@store')->name('calendar_store');

    
    
    Route::group(['prefix' => 'feedbacks'], function () {
        Route::get('/', 'PagesController@feedback')->name('feedback');
    });

});



// adminlogin
Route::get('/admin/login', 'UserController@getloginadmin');
Route::post('/admin/login', 'UserController@postloginadmin')->name('admin_login');
Route::get('admin/logout', 'UserController@logoutadmin');

Route::group(['prefix' => 'admin', 'middleware' => 'is_admin'], function () {
    
    Route::get('/dashboard', function () {
        return view('be/layouts/dashboard');
    });
    
    Route::get('table', function () {
        return view('be/user/list');
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('list', 'UserController@index');
        Route::get('add', 'UserController@create');
        Route::post('add', 'UserController@store')->name('user_add');
        Route::get('edit/{id}', 'UserController@edit');
        Route::post('edit/{id}', 'UserController@update')->name('user_edit');
        Route::get('delete/{id}', 'UserController@destroy')->name('user_destroy');
    });
    
    Route::group(['prefix' => 'schools'], function () {
        Route::get('list', 'SchoolController@index')->name('schools_show');
        Route::get('add', 'SchoolController@create')->name('schools_create');
        Route::post('add', 'SchoolController@store')->name('school_store');
        Route::get('edit/{id}', 'SchoolController@edit')->name('school_edit');
        Route::post('update/{id}', 'SchoolController@update')->name('school_update');
        Route::get('delete/{id}', 'SchoolController@destroy')->name('school_destroy');
    });
    
    
    Route::group(['prefix' => 'feedbacks'], function () {
        Route::get('list', 'FeedbackController@index')->name('feedbacks_show');
        Route::get('add', 'FeedbackController@create')->name('feedbacks_create');
        Route::post('store', 'FeedbackController@store')->name('feedback_store');
        Route::get('edit/{id}', 'FeedbackController@edit')->name('feedback_edit');
        Route::post('update', 'FeedbackController@update')->name('feedback_update');
        Route::get('delete/{id}', 'FeedbackController@destroy')->name('feedback_destroy');
    });
    Route::group(['prefix' => 'programs'], function () {
        Route::get('list', 'ProgramController@index')->name('programs_show');
        Route::get('add', 'ProgramController@create')->name('programs_create');
        Route::post('add', 'ProgramController@store')->name('program_store');
        Route::get('edit/{id}', 'ProgramController@edit')->name('program_edit');
        Route::post('update/{id}', 'ProgramController@update')->name('program_update');
        Route::get('delete/{id}', 'ProgramController@destroy')->name('program_destroy');
    });
    
    Route::group(['prefix' => 'subjects'], function () {
        Route::get('list', 'SubjectController@index')->name('subjects_show');
        Route::get('add', 'SubjectController@create')->name('subjects_create');
        Route::post('add', 'SubjectController@store')->name('subject_store');
        Route::get('edit/{id}', 'SubjectController@edit')->name('subject_edit');
        Route::post('update/{id}', 'SubjectController@update')->name('subject_update');
        Route::get('delete/{id}', 'SubjectController@destroy')->name('subject_destroy');
    });
    
    Route::group(['prefix' => 'staffs'], function () {
        Route::get('list', 'StaffSchoolController@index')->name('staffs_show');
        Route::get('add', 'StaffSchoolController@create')->name('staffs_create');
        Route::post('add', 'StaffSchoolController@store')->name('staff_store');
        Route::get('edit/{id}', 'StaffSchoolController@edit')->name('staff_edit');
        Route::post('update/{id}', 'StaffSchoolController@update')->name('staff_update');
        Route::get('delete/{id}', 'StaffSchoolController@destroy')->name('staff_destroy');
    });
    Route::group(['prefix' => 'monthly_rp'], function () {
        Route::get('list', 'MonthlyRpController@index')->name('monthly_rp_show');
        Route::get('add', 'MonthlyRpController@create')->name('monthly_rp_create');
        Route::post('add', 'MonthlyRpController@store')->name('monthly_rp_store');
        Route::get('edit/{id}', 'MonthlyRpController@edit')->name('monthly_rp_edit');
        Route::post('update/{id}', 'MonthlyRpController@update')->name('monthly_rp_update');
        Route::get('delete/{id}', 'MonthlyRpController@destroy')->name('monthly_rp_destroy');
    });
    Route::group(['prefix' => 'class'], function () {
        Route::get('list', 'UnitClassController@index')->name('class_show');
        Route::get('add', 'UnitClassController@create')->name('class_create');
        Route::post('add', 'UnitClassController@store')->name('class_store');
        Route::get('edit/{id}', 'UnitClassController@edit')->name('class_edit');
        Route::post('update/{id}', 'UnitClassController@update')->name('class_update');
        Route::get('delete/{id}', 'UnitClassController@destroy')->name('class_destroy');
    });
    Route::group(['prefix' => 'students'], function () {
        Route::get('list', 'StudentController@index')->name('students_show');
        Route::get('add', 'StudentController@create')->name('student_create');
        Route::post('add', 'StudentController@store')->name('student_store');
        Route::get('edit/{id}', 'StudentController@edit')->name('student_edit');
        Route::post('update/{id}', 'StudentController@update')->name('student_update');
        Route::get('delete/{id}', 'StudentController@destroy')->name('student_destroy');
    });
    
    Route::group(['prefix' => 'teachers'], function () {
        Route::get('list', 'TeacherController@index')->name('teachers_show');
        Route::get('add', 'TeacherController@create')->name('teacher_create');
        Route::post('add', 'TeacherController@store')->name('teacher_store');
        Route::post('info_add', 'TeacherController@info_store')->name('info_store');
        Route::get('edit/{id}', 'TeacherController@edit')->name('teacher_edit');
        Route::post('edit/{id}', 'TeacherController@update')->name('teacher_update');
        Route::get('delete/{id}', 'TeacherController@destroy')->name('teacher_destroy');
    });

    Route::group(['prefix' => 'schedules'], function () {
        Route::get('list', 'ScheduleController@index');
        Route::get('add', 'ScheduleController@create');
        Route::post('add', 'ScheduleController@store')->name('schedule_add');
        Route::get('edit/{id}', 'ScheduleController@edit');
        Route::post('edit/{id}', 'ScheduleController@update')->name('schedule_edit');
        Route::get('delete/{id}', 'ScheduleController@destroy')->name('schedule_destroy');
        Route::get('booking', 'ScheduleController@booking');
        Route::post('booking_result', 'ScheduleController@result_booking')->name('result_booking');
        Route::post('add_booking', 'ScheduleController@add_booking')->name('add_booking');
        Route::get('schedules', 'ScheduleController@schedules')->name('list_booking');
        Route::post('cancel_booking', 'ScheduleController@cancel_booking')->name('cancel_booking');
        
    });

    Route::group(['prefix' => 'evaluation'], function () {
        
        
        Route::get('json', 'EvaluationController@json');

        Route::get('leuleu', function () {
            return view('be/charts/test_charts');
        });

        Route::get('test', function () {
            return view('be/evaluation/testjson');
        });
        Route::post('test', 'EvaluationController@test')->name('test');
        Route::get('draft', 'EvaluationController@index');
        Route::post('completed_evaluation', 'EvaluationController@completed_evaluation')->name('completed_evaluation');
        Route::get('completed', 'EvaluationController@completed');
        Route::get('show/{id}', 'EvaluationController@show')->name('evaluation_show');
        Route::get('view/{id}', 'EvaluationController@view');
        Route::post('save_evaluation', 'EvaluationController@store')->name('evaluation_save');
        Route::get('add', 'EvaluationController@create');
        Route::post('add', 'EvaluationController@store')->name('evaluation_add');
        Route::get('edit/{id}', 'EvaluationController@edit');
        Route::post('edit/{id}', 'EvaluationController@update')->name('evaluation_update');
        Route::get('delete/{id}', 'EvaluationController@destroy')->name('evaluation_destroy');
        
        Route::get('ranking', 'EvaluationController@ranking')->name('ranking');
        Route::post('ranking_result', 'EvaluationController@ranking_result')->name('ranking_result');
        Route::get('chart_search', 'EvaluationController@chart_search')->name('chart_search');
        Route::post('chart_location_result', 'EvaluationController@chart')->name('chart_location_result');

        // Route::get('chart', 'EvaluationController@chart')->name('chart');
        Route::get('chart_location', 'EvaluationController@chart_location')->name('chart_location');
    });

    Route::get('export', 'ExcelController@export')->name('export');
    Route::get('excel', 'ExcelController@importExportView');
    Route::post('import', 'ExcelController@import')->name('import');
});
