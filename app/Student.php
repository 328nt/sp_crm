<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    
    public function class()
    {
        return $this->belongsTo('App\Unit_class', 'unit_class_id', 'id');
    }
    public function school()
    {
        return $this->belongsTo('App\School', 'school_id', 'id');
    }
    public function comment()
    {
        return $this->hasMany('App\Student_comment', 'student_id', 'id');
    }
}
