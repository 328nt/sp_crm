<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event_calendar extends Model
{
    protected $fillable = ['title', 'start_date', 'end_date'];
}
