<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    public function team()
    {
        return $this->belongsTo('App\Team', 'team_id', 'id');
    }
    public function topic()
    {
        return $this->belongsTo('App\Topic', 'topic_id', 'id');
    }
    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id', 'id');
    }
}
