<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit_class extends Model
{
    public function school()
    {
        return $this->belongsTo('App\School', 'school_id', 'id');
    }
    public function program()
    {
        return $this->belongsTo('App\Program', 'program_id', 'id');
    }
    public function student()
    {
        return $this->hasMany('App\Student', 'unit_class_id', 'id');
    }
    public function comment()
    {
        return $this->hasMany('App\Comment_class', 'unit_class_id', 'id');
    }
}
