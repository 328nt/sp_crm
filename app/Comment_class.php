<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment_class extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function class()
    {
        return $this->belongsTo('App\Unit_class', 'unit_class_id', 'id');
    }
}
