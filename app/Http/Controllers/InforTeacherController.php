<?php

namespace App\Http\Controllers;

use App\Infor_teacher;
use Illuminate\Http\Request;

class InforTeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Infor_teacher  $infor_teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Infor_teacher $infor_teacher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Infor_teacher  $infor_teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Infor_teacher $infor_teacher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Infor_teacher  $infor_teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Infor_teacher $infor_teacher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Infor_teacher  $infor_teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Infor_teacher $infor_teacher)
    {
        //
    }
}
