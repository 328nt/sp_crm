<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\Topic;
use App\Status;
use Auth;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $statuses = Status::all();
        $feedbacks = Feedback::all();
        return view('be.feedback.list', compact('feedbacks', 'statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $fbs = new Feedback();
        $fbs->user_id = Auth::user()->id;
        $fbs->team_id = $rq->team_id;
        $fbs->topic_id = $rq->topic;
        $fbs->deadline = date('Y/m/d', strtotime($rq->deadline));
        $fbs->serious = $rq->serious;
        $fbs->content = $rq->content;
        $fbs->status_id = 2;
        // $all = $rq->all();
        // dd($fbs->deadline);
        $fbs->save();
        return redirect()->back()->with('msg', 'Sending Feedback');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function show(Feedback $feedback)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function edit(Feedback $feedback)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq)
    {
        $feedback = Feedback::find($rq->id);
        $feedback->status_id = $rq->status_id;
        $feedback->note = $rq->note;
        // dd($feedback);
        $feedback->save();
        return redirect()->back()->with('msg', 'oklah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feedback $feedback)
    {
        //
    }
}
