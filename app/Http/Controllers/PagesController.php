<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\School;
use App\Teacher;
use App\Staff_school;
use App\Unit_class;
use App\Student;
use App\Student_comment;
use App\Test;
use App\Subject;
use App\Event_calendar;
use App\User;
use App\Team;
use App\Feedback;
use App\Topic;
use App\Status;
use Calendar;
use Carbon\Carbon;
use MaddHatter\LaravelFullcalendar\Event;

class PagesController extends Controller
{

    public function getloginmanager()
    {
        return view('pages/login');
    }

    public function postloginmanager(Request $rq)
    {
        $this->validate($rq,[
            'email' => 'required',
            'pwd' => 'required',

        ],[
            'pwd.required' =>'please insert password',
            'email.required' =>'please insert email',

        ]);
        // dd($rq->pwd);
        if (Auth::attempt(['email' => $rq->email, 'password' => $rq->pwd])) {
            return redirect()->route('teacher_info');
        } else {
            return redirect()->route('get_login')->with('msg','wrong email or password!, please try again.');
        }
        
    }
    public function logoutmanager()
    {
        Auth::logout();
        return redirect()->route('get_login')->with('msg','Đăng xuất thành công bạn ôi !');
    }
    
    public function select_school()
    {
        $schools = School::all();
        return view('pages/school_select', compact('schools'));
    }
    
    public function school_selected(Request $rq)
    {
        $id = $rq->school;
        $school = School::find($id);
        $slug_name = $school->slug_name;
        return redirect()->route('school_info', [$id, $slug_name]);
    }
    
    public function teacher_info()
    {
        $user_id = Auth::user()->id;
        $events = [];
        $data = Event_calendar::where('user_id', $user_id)->get();
        // $data = Event_calendar::first();
        // $start_date =$data->start_date;
        // $date = Carbon::parse($start_date)->addWeek();
        // dd($date);



        if($data->count()) {
            foreach ($data as $key => $value) {
                $start_date =$value->start_date;
                // $start_date = Carbon::parse($start_date);
                $end_date =$value->end_date.' +1 day';
                // $end_date = Carbon::parse($end_date);
                    // for( $i = 0; $i <= 3; $i++ ){
                    //     $start_date = $start_date->addWeek(1);
                    //     // dd($start_date);
                    //     $end_date = $end_date->addWeek(1);
                    $events[] = Calendar::event(
                        $value->title,
                        true,
                        new \DateTime($start_date),
                        new \DateTime($end_date),
                        null,
                        // Add color and link on event
                    [
                        // 'start_time' => $start_date.'+T17:30:00',
                        // 'end_time' => $start_date.'+T18:30:00',
                        'color' => $value->color,
                        'url' => $value->url,
                    ]
                    );
                // }
            }
        }
        $calendar = Calendar::addEvents($events);
        // dd($calendar->event());
    
        $id = Auth::user()->id;
        $teacher = User::find($id);
        // $school = School::find($id);
        // $staffs = Staff_school::where('school_id', $id)->get();
        // $classes = Unit_class::where('school_id', $id)->get();
        // dd($staffs);
        return view('pages/teacher_info', compact('teacher', 'calendar'));
    }

    public function school_info($id)
    {
        $school = School::find($id);
        $staffs = Staff_school::where('school_id', $id)->get();
        $classes = Unit_class::where('school_id', $id)->get();
        // dd($staffs);
        return view('pages/school_info', compact('school', 'staffs', 'classes'));
    }
    public function class_info($id)
    {
        $class = Unit_class::find($id);
        // dd($class->comment);
        return view('pages/class_info', compact('class'));
    }

    public function student_info($id)
    {
        $student = Student::find($id);
        $subjects = Subject::all();
        $cm = $student->comment;
        // dd($cm);
        return view('pages/student_info', compact('student', 'subjects'));
    }
    
    public function score_student(Request $rq, $id)
    {
        $student_comment = new Student_comment();
        $student_comment->student_id = $id;
        $student_comment->user_id = Auth::user()->id;
        $student_comment->point = $rq->point;
        $student_comment->comment = $rq->comment;
        // dd($student_comment);
        $student_comment->save();
        return redirect()->back()->with('msg', 'Add score success!');
    }
    
    
    public function student_test(Request $rq, $id)
    {
        $student_test = new Test();
        $student_test->student_id = $id;
        $student_test->subject_id = $rq->subject_id;
        $student_test->user_id = Auth::user()->id;
        $student_test->score = $rq->score;
        $student_test->comment = $rq->comment;
        dd($student_test);
        $student_test->save();
        return redirect()->back()->with('msg', 'Add score success!');
    }
    
    public function add_new_school(Request $rq)
    {
        $this->validate($rq,[
            'name' => 'required|max:100',
            'address' => 'required'
        ],[
            'name.required' =>'please insert name',
            'address.required' =>'please insert address'
        ]);
            $school = new School();
            $school->name = $rq->name;
            $school->slug_name = str_slug($rq->name);
            $school->address = $rq->address;
            $school->founding = $rq->founding;
            $school->event = $rq->event;
            if ($rq->hasFile('image')) {
                $file = $rq->file('image');
                $name = $file->getClientOriginalName();
                $img = str_random(4)."-".$name;
                while (file_exists("upload/schools/".$img)){
                    $img = str_random(4)."-".$name;
                }
                $file->move("upload/schools", $img);
                $school->image = $img;
            } else {
                $school->image = 'default.png';
            }
            // dd($school);
            $school->save();
            return redirect()->back()->with('msg', 'Create new school success!');
    }
    
    public function feedback()
    {
        $user = User::find(Auth::user()->id);
        $teams = Team::all();
        $topics = Topic::all();
        $feedbacks = Feedback::all();
        return view('pages/feedback', compact('teams', 'topics', 'feedbacks', 'user'));
    }
}
