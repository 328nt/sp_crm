<?php

namespace App\Http\Controllers;

use App\Teacher;
use App\School;
use App\User;
use App\Infor_teacher;
use Auth;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = User::where('role', 3)->get();
        return view('be/teachers/list', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schools = School::all();
        return view('be/teachers/add', compact('schools'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $this->validate($rq,[
            'name' => 'required|max:500',
            'email' => 'required',
            'pwd' => 'required',
            'repwd' => 'required| same:pwd',
        ],[
            'name.required' =>'please insert name',
            'email.required' =>'please insert email',
            'pwd.required' => 'please insert password',
            'repwd.required' => 'please insert Re-password',
            'repwd.same' => 'nhập lại mà cũng sai. -_-',
        ]);
        // try {
            $teacher = new User();
            $teacher->name = $rq->name;
            $teacher->email = $rq->email;
            $teacher->mobile = $rq->mobile;
            $teacher->role = 3;
            $teacher->password = bcrypt($rq->pwd);
            if ($rq->hasFile('image')) {
                $file = $rq->file('image');
                $name = $file->getClientOriginalName();
                $img = str_random(4)."-".$name;
                while (file_exists("upload/users/".$img)){
                    $img = str_random(4)."-".$name;
                }
                $file->move("upload/users", $img);
                $teacher->image = $img;
            } else {
                $teacher->image = 'default.png';
            }
            // dd($teacher);
            $teacher->save();
            return redirect()->route('teachers_show', [])->with('msg','create new success !');
            // return redirect('admin/teachers/list')->with('msg','oklah');
        // } catch (\Exception  $ex) {
        //     return back()->withErrors($ex->getMessage());
        // }
    }
    public function info_store(Request $rq)
    {
        $this->validate($rq,[
            'school_id' => 'required',
        ],[
            'school_id.required' =>'please select school',
        ]);
            // $info = new Infor_teacher();
            $info = $rq->all();
            Infor_teacher::create($info);
            // dd($info);
            return redirect()->back()->with('msg', 'create info teacher success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = User::find($id);
        $schools = School::all();
        return view('be/teachers/edit',compact('teacher', 'schools'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        $this->validate($rq,[
            'name' => 'required|max:500',
            'email' => 'required',
        ],[
            'name.required' =>'please insert name',
            'email.required' =>'please insert email',
        ]);
        // try {
            // dd($rq->image);
            $teacher = User::find($id);
            $teacher->name = $rq->name;
            $teacher->email = $rq->email;
            $teacher->mobile = $rq->mobile;
            if ($rq->hasFile('image')) {
                $file = $rq->file('image');
                $name = $file->getClientOriginalName();
                $img = str_random(4)."-".$name;
                while (file_exists("upload/users/".$img)){
                    $img = str_random(4)."-".$name;
                }
                $file->move("upload/users", $img);
                // unlink("upload/users/".$teacher->image);
                $teacher->image = $img;
            }
            
            
            $info = new Infor_teacher();
            $info->user_id = $id;
            dd($info);
            $teacher->save();
            return redirect()->route('teachers_show', [])->with('msg','update success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teacher = Teacher::find($id);
        $teacher->delete();
        return redirect()->route('students_show', [])->with('msg','Delete success !');
    }
}
