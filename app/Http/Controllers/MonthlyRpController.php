<?php

namespace App\Http\Controllers;

use App\Monthly_rp;
use App\School;
use Illuminate\Http\Request;

class MonthlyRpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $file_rp = Monthly_rp::all();
        return view('be/monthly_rp/list', compact('file_rp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schools = School::all();
        return view('be/monthly_rp/add', compact('schools'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $this->validate($rq,[
            'name' => 'required|max:100',
        ],[
            'name.required' =>'please insert name',
            // 'pwd.required' => 'please insert password',
            // 'repwd.required' => 'please insert Re-password',
            // 'repwd.same' => 'nhập lại mà cũng sai. -_-',
        ]);
            $file_rp = new Monthly_rp();
            $file_rp->name = $rq->name;
            $file_rp->school_id = $rq->school_id;
            $file_rp->category = $rq->category;
            $file_rp->note = $rq->note;
            if ($rq->hasFile('file')) {
                $file = $rq->file('file');
                $name = $file->getClientOriginalName();
                $img = str_random(4)."-".$name;
                while (file_exists("upload/monthly_rp/".$img)){
                    $img = str_random(4)."-".$name;
                }
                $file->move("upload/monthly_rp", $img);
                $file_rp->file = $img;
            } else {
                $file_rp->file = '';
            }
            $file_rp->save();
            return redirect()->route('monthly_rp_show', [])->with('msg','create new success !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Monthly_rp  $monthly_rp
     * @return \Illuminate\Http\Response
     */
    public function show(Monthly_rp $monthly_rp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Monthly_rp  $monthly_rp
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schools = School::all();
        $file = Monthly_rp::find($id);
        return view('be/monthly_rp/edit',compact('file','schools'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Monthly_rp  $monthly_rp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        $this->validate($rq,[
            'name' => 'required|max:100',
        ],[
            'name.required' =>'please insert name',
            // 'pwd.required' => 'please insert password',
            // 'repwd.required' => 'please insert Re-password',
            // 'repwd.same' => 'nhập lại mà cũng sai. -_-',
        ]);
            $file_rp = Monthly_rp::find($id);
            $file_rp->name = $rq->name;
            $file_rp->school_id = $rq->school_id;
            $file_rp->category = $rq->category;
            $file_rp->note = $rq->note;
            if ($rq->hasFile('file')) {
                $file = $rq->file('file');
                $name = $file->getClientOriginalName();
                $img = str_random(4)."-".$name;
                while (file_exists("upload/monthly_rp/".$img)){
                    $img = str_random(4)."-".$name;
                }
                $file->move("upload/monthly_rp", $img);
                unlink("upload/monthly_rp/".$file_rp->file);
                $file_rp->file = $img;
            }
            $file_rp->save();
            return redirect()->route('monthly_rp_show', [])->with('msg',' edit file success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Monthly_rp  $monthly_rp
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = Monthly_rp::find($id);
        $file->delete();
        return redirect()->route('monthly_rp_show', [])->with('msg','Delete success !');
    }
}
