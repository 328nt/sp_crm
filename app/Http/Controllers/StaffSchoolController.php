<?php

namespace App\Http\Controllers;

use App\Staff_school;
use App\School;
use Illuminate\Http\Request;

class StaffSchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staffs = Staff_school::all();
        return view('be/staffs/list', compact('staffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schools = School::all();
        return view('be/staffs/add', compact('schools'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $this->validate($rq,[
            'name' => 'required|max:100',
        ],[
            'name.required' =>'please insert name',
            // 'pwd.required' => 'please insert password',
            // 'repwd.required' => 'please insert Re-password',
            // 'repwd.same' => 'nhập lại mà cũng sai. -_-',
        ]);
            $staff = new Staff_school();
            $staff->name = $rq->name;
            $staff->school_id = $rq->school_id;
            $staff->dob = $rq->dob;
            $staff->position = $rq->position;
            $staff->phone = $rq->phone;
            $staff->email = $rq->email;
            $staff->charge_of = $rq->charge_of;
            if ($rq->hasFile('image')) {
                $file = $rq->file('image');
                $name = $file->getClientOriginalName();
                $img = str_random(4)."-".$name;
                while (file_exists("upload/staffs/".$img)){
                    $img = str_random(4)."-".$name;
                }
                $file->move("upload/staffs", $img);
                $staff->image = $img;
            } else {
                $staff->image = 'default.png';
            }
            $staff->save();
            return redirect()->route('staffs_show', [])->with('msg','create new success !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Staff_school  $staff_school
     * @return \Illuminate\Http\Response
     */
    public function show(Staff_school $staff_school)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Staff_school  $staff_school
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schools = School::all();
        $staff = Staff_school::find($id);
        return view('be/staffs/edit',compact('staff','schools'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Staff_school  $staff_school
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        $this->validate($rq,[
            'name' => 'required|max:100',
        ],[
            'name.required' =>'please insert name',
            // 'pwd.required' => 'please insert password',
            // 'repwd.required' => 'please insert Re-password',
            // 'repwd.same' => 'nhập lại mà cũng sai. -_-',
        ]);
            $staff = Staff_school::find($id);
            $staff->name = $rq->name;
            $staff->school_id = $rq->school_id;
            $staff->dob = $rq->dob;
            $staff->position = $rq->position;
            $staff->phone = $rq->phone;
            $staff->email = $rq->email;
            $staff->charge_of = $rq->charge_of;
            if ($rq->hasFile('image')) {
                $file = $rq->file('image');
                $name = $file->getClientOriginalName();
                $img = str_random(4)."-".$name;
                while (file_exists("upload/staffs/".$img)){
                    $img = str_random(4)."-".$name;
                }
                $file->move("upload/staffs", $img);
                unlink("upload/staffs/".$staff->image);
                $staff->image = $img;
            }
            $staff->save();
            return redirect()->route('staffs_show', [])->with('msg','update success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Staff_school  $staff_school
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staff = Staff_school::find($id);
        $staff->delete();
        return redirect()->route('staffs_show', [])->with('msg','Delete success !');
    }
}
