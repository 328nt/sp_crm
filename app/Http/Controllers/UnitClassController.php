<?php

namespace App\Http\Controllers;

use App\Unit_class;
use App\School;
use Illuminate\Http\Request;

class UnitClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = Unit_class::all();
        return view('be/class/list', compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schools = School::all();
        return view('be/class/add', compact('schools'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $this->validate($rq,[
            'name' => 'required|max:100',
            'school_id' => 'required',
        ],[
            'name.required' =>'please insert name',
            'school_id.required' =>'please select school',
            // 'pwd.required' => 'please insert password',
            // 'repwd.required' => 'please insert Re-password',
            // 'repwd.same' => 'nhập lại mà cũng sai. -_-',
        ]);
            $class = new Unit_class();
            $class->name = $rq->name;
            $class->slug_name = str_slug($rq->name);
            $class->school_id = $rq->school_id;
            $class->des = $rq->des;
            $class->info = $rq->info;
            if ($rq->hasFile('image')) {
                $file = $rq->file('image');
                $name = $file->getClientOriginalName();
                $img = str_random(4)."-".$name;
                while (file_exists("upload/classes/".$img)){
                    $img = str_random(4)."-".$name;
                }
                $file->move("upload/classes", $img);
                $class->image = $img;
            } else {
                $class->image = 'default.png';
            }
            $class->save();
            return redirect()->route('class_show', [])->with('msg','create new success !');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Unit_class  $unit_class
     * @return \Illuminate\Http\Response
     */
    public function show(Unit_class $unit_class)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Unit_class  $unit_class
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schools = School::all();
        $class = Unit_class::find($id);
        return view('be/class/edit',compact('class','schools'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unit_class  $unit_class
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        $this->validate($rq,[
            'name' => 'required|max:100',
        ],[
            'name.required' =>'please insert name',
            // 'pwd.required' => 'please insert password',
            // 'repwd.required' => 'please insert Re-password',
            // 'repwd.same' => 'nhập lại mà cũng sai. -_-',
        ]);
            $class = Unit_class::find($id);
            $class->school_id = $rq->school_id;
            $class->name = $rq->name;
            $class->slug_name = str_slug($rq->name);
            $class->des = $rq->des;
            $class->info = $rq->info;
            if ($rq->hasFile('image')) {
                $file = $rq->file('image');
                $name = $file->getClientOriginalName();
                $img = str_random(4)."-".$name;
                while (file_exists("upload/classes/".$img)){
                    $img = str_random(4)."-".$name;
                }
                $file->move("upload/classes", $img);
                // unlink("upload/classes/".$class->image);
                $class->image = $img;
            }
            $class->save();
            return redirect()->route('class_show', [])->with('msg',' edit class success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unit_class  $unit_class
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $class = Unit_class::find($id);
        $class->delete();
        return redirect()->route('class_show', [])->with('msg','Delete success !');
    }
}
