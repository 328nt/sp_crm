<?php

namespace App\Http\Controllers;

use App\School;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schools = School::all();
        return view('be/schools/list', compact('schools'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('be/schools/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $this->validate($rq,[
            'name' => 'required|max:100',
            'image' => 'required',
            'address' => 'required',
            // 'pwd' => 'required',
            // 'repwd' => 'required| same:pwd',
        ],[
            'name.required' =>'please insert name',
            'image.required' =>'please insert image',
            'address.required' =>'please insert address',
            // 'pwd.required' => 'please insert password',
            // 'repwd.required' => 'please insert Re-password',
            // 'repwd.same' => 'nhập lại mà cũng sai. -_-',
        ]);
            $school = new School();
            $school->name = $rq->name;
            $school->slug_name = str_slug($rq->name);
            $school->address = $rq->address;
            $school->founding = $rq->founding;
            $school->event = $rq->event;
            if ($rq->hasFile('image')) {
                $file = $rq->file('image');
                $name = $file->getClientOriginalName();
                $img = str_random(4)."-".$name;
                while (file_exists("upload/schools/".$img)){
                    $img = str_random(4)."-".$name;
                }
                $file->move("upload/schools", $img);
                $school->image = $img;
            } else {
                $school->image = 'default.png';
            }
            if ($rq->hasFile('daily_rp')) {
                $getfile = $rq->file('daily_rp');
                $name = $getfile->getClientOriginalName();
                $file = str_random(4)."-".$name;
                while (file_exists("upload/schools/".$file)){
                    $file = str_random(4)."-".$name;
                }
                $file->move("upload/schools", $file);
                $school->daily_rp = $file;
            } else {
                $school->daily_rp = 'default.png';
            }
            // dd($school);
            $school->save();
            return redirect()->route('schools_show', [])->with('msg','create new success !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function show(School $school)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $school = School::find($id);
        return view('be/schools/edit',compact('school'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        $this->validate($rq,[
            'name' => 'required|max:100',
            'address' => 'required',
            // 'pwd' => 'required',
            // 'repwd' => 'required| same:pwd',
        ],[
            'name.required' =>'please insert name',
            'address.required' =>'please insert address',
            // 'pwd.required' => 'please insert password',
            // 'repwd.required' => 'please insert Re-password',
            // 'repwd.same' => 'nhập lại mà cũng sai. -_-',
        ]);
        // try {
            $school = School::find($id);
            $school->name = $rq->name;
            $school->address = $rq->address;
            $school->founding = $rq->founding;
            $school->event = $rq->event;
            if ($rq->hasFile('image')) {
                $file = $rq->file('image');
                $name = $file->getClientOriginalName();
                $img = str_random(4)."-".$name;
                while (file_exists("upload/schools/".$img)){
                    $img = str_random(4)."-".$name;
                }
                $file->move("upload/schools", $img);
                // unlink("upload/schools/".$school->image);
                $school->image = $img;
            }
            if ($rq->hasFile('daily_rp')) {
                $getfile = $rq->file('daily_rp');
                $name = $getfile->getClientOriginalName();
                $file = str_random(4)."-".$name;
                while (file_exists("upload/schools/".$file)){
                    $file = str_random(4)."-".$name;
                }
                $file->move("upload/schools", $file);
                unlink("upload/schools/".$school->daily_rp);
                $school->daily_rp = $file;
            }
            $school->save();
            return redirect()->route('schools_show', [])->with('msg','update success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $school = School::find($id);
        $school->delete();
        return redirect()->route('schools_show', [])->with('msg','Delete success !');
    }
}
