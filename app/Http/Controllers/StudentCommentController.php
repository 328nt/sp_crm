<?php

namespace App\Http\Controllers;

use App\Student_comment;
use Illuminate\Http\Request;

class StudentCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student_comment  $student_comment
     * @return \Illuminate\Http\Response
     */
    public function show(Student_comment $student_comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student_comment  $student_comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Student_comment $student_comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student_comment  $student_comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student_comment $student_comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student_comment  $student_comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student_comment $student_comment)
    {
        //
    }
}
