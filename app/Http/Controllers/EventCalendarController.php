<?php

namespace App\Http\Controllers;

use App\Event_calendar;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use Illuminate\Http\Request;
use Auth;

class EventCalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = [];
        $data = Event_calendar::all();
        if($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    // Add color and link on event
                [
                    'color' => '##933EC5',
                    'url' => $value->title,
                    'dow' => [ 5 ]
                ]
                );
            }
        }
        $calendar = Calendar::addEvents($events)->weekly();
        return view('fullcalender', compact('calendar'));
    }
    public function datatable()
    {
        return view('datatable');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    //$type=Input::get('type');
    $event_id= Input::get('eventid');
    $title= Input::get('title');
    $start_day=Input::get('start');
    $end_day=Input::get('end');
    $allday=Input::get('allday');
    $repeat=Input::get('repeat');
    $frequency=Input::get('frequency');
    $start_time=Input::get('start_time');
    $end_time=Input::get('end_time');   
    $dow=Input::get('dow');
    $month=Input::get('month');
    $weekly_json=json_encode($dow);
    $monthly_json=json_encode($month);          
    $newstrt=substr($start_day,0,10);
    $newend=substr($end_day,0,10);
    $start= date("Y-m-d H:i:s",$newstrt);
    $end= date("Y-m-d H:i:s" , $newend);
    $roles = DB::table('events')
                ->where('event_id','=',$event_id)
                ->update(array('title' => $title,'daily'=>$allday,'repeat'=>$repeat,'frequency'=>$frequency,'start'=>$start,'end'=>$end,'time'=>$time,'dow'=>$weekly_json,'monthly_json'=>$monthly_json));

    if (Request::ajax())
    { 

    return Response::json(array('id'=>$event_id,'title'=>$title,'newstrt'=>$start,'newend'=>$end,'start_time'=>$start_time,'end_time'=>$end_time));
    }               
    else 
    {
        return  Redirect::route('calendar.index');
    }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $event = new Event_calendar();
        $event->user_id = Auth::user()->id;
        $event->title = $rq->title;
        $event->color = $rq->color;
        $event->start_date = date('Y-m-d', strtotime($rq->start_date));
        $event->end_date = date('Y-m-d', strtotime($rq->end_date));
        // dd($event);
        $event->save();
        return redirect()->back()->with('msg', 'Create new event success!!!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event_calendar  $event_calendar
     * @return \Illuminate\Http\Response
     */
    public function show(Event_calendar $event_calendar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event_calendar  $event_calendar
     * @return \Illuminate\Http\Response
     */
    public function edit(Event_calendar $event_calendar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event_calendar  $event_calendar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event_calendar $event_calendar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event_calendar  $event_calendar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event_calendar $event_calendar)
    {
        //
    }
}
