<?php

namespace App\Http\Controllers;

use App\Student;
use App\School;
use App\Unit_class;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();
        return view('be/students/list', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schools = School::all();
        $classes = Unit_class::all();
        return view('be/students/add', compact('schools', 'classes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $this->validate($rq,[
            // 'school_id' => 'required',
            'unit_class_id' => 'required',
            'fullname' => 'required|max:100',
        ],[
            'fullname.required' =>'please insert fullname',
            // 'school_id.required' =>'please select school',
            'unit_class_id.required' =>'please select class',
            // 'pwd.required' => 'please insert password',
            // 'repwd.required' => 'please insert Re-password',
            // 'repwd.same' => 'nhập lại mà cũng sai. -_-',
        ]);
            $student = new Student();
            $student->fullname = $rq->fullname;
            // $student->school_id = $rq->school_id;
            $student->unit_class_id = $rq->unit_class_id;
            $student->school_id = Unit_class::find($rq->unit_class_id)->school_id;
            $student->dob = $rq->dob;
            $student->dad_name = $rq->dad_name;
            $student->dad_phone = $rq->dad_phone;
            $student->dad_email = $rq->dad_email;
            $student->mom_name = $rq->mom_name;
            $student->mom_phone = $rq->mom_phone;
            $student->mom_email = $rq->mom_email;
            if ($rq->hasFile('image')) {
                $file = $rq->file('image');
                $name = $file->getClientOriginalName();
                $img = str_random(4)."-".$name;
                while (file_exists("upload/students/".$img)){
                    $img = str_random(4)."-".$name;
                }
                $file->move("upload/students", $img);
                $student->image = $img;
            } else {
                $student->image = 'default.png';
            }
            // dd($student);
            $student->save();
            return redirect()->route('students_show', [])->with('msg','create new success !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schools = School::all();
        $classes = Unit_class::all();
        $student = Student::find($id);
        return view('be/students/edit',compact('student','schools', 'classes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        $this->validate($rq,[
            'fullname' => 'required|max:100',
        ],[
            'fullname.required' =>'please insert name',
            // 'pwd.required' => 'please insert password',
            // 'repwd.required' => 'please insert Re-password',
            // 'repwd.same' => 'nhập lại mà cũng sai. -_-',
        ]);
            $student = Student::find($id);
            $student->fullname = $rq->fullname;
            // $student->school_id = $rq->school_id;
            $student->unit_class_id = $rq->unit_class_id;
            $student->school_id = Unit_class::find($rq->unit_class_id)->school_id;
            $student->dob = $rq->dob;
            $student->dad_name = $rq->dad_name;
            $student->dad_phone = $rq->dad_phone;
            $student->dad_email = $rq->dad_email;
            $student->mom_name = $rq->mom_name;
            $student->mom_phone = $rq->mom_phone;
            $student->mom_email = $rq->mom_email;
            if ($rq->hasFile('image')) {
                $file = $rq->file('image');
                $name = $file->getClientOriginalName();
                $img = str_random(4)."-".$name;
                while (file_exists("upload/students/".$img)){
                    $img = str_random(4)."-".$name;
                }
                $file->move("upload/students", $img);
                unlink("upload/students/".$student->image);
                $student->image = $img;
            }
            // dd($student);
            $student->save();
            return redirect()->route('students_show', [])->with('msg','update success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);
        $student->delete();
        return redirect()->route('students_show', [])->with('msg','Delete success !');
    }
}
