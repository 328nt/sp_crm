<?php

namespace App\Http\Controllers;

use App\Comment_class;
use Auth;
use Illuminate\Http\Request;

class CommentClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq, $id)
    {
        $comment_class = new Comment_class();
        $comment_class->unit_class_id = $id;
        $comment_class->user_id = Auth::user()->id;
        $comment_class->comment = $rq->comment;
        // dd($comment_class);
        $comment_class->save();
        return redirect()->back()->with('msg', 'add comment success!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment_class  $comment_class
     * @return \Illuminate\Http\Response
     */
    public function show(Comment_class $comment_class)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment_class  $comment_class
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment_class $comment_class)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment_class  $comment_class
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment_class $comment_class)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment_class  $comment_class
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment_class $comment_class)
    {
        //
    }
}
