<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public function program()
    {
        return $this->belongsTo('App\Program', 'program_id', 'id');
    }
}
